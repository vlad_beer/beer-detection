//
//  SettingsViewController.swift
//  YOLOv3-CoreML
//
//  Created by Vladislav Shakhray on 05.03.2020.
//  Copyright © 2020 MachineThink. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    var caller: ViewController? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func reset() {
        caller!.yolo.modelType = .spp
        caller!.yolo.confidenceThreshold = YOLO.defaultConfidenceThreshold
        caller!.displayExtraBoxes = false
        caller!.enableIdentification = false
        caller!.identificationThreshold = ViewController.defaultIdentificationThreshold
        caller!.saveErrors = false
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(self.reset))
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            return 3
        case 1:
            return 2
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    @objc func changedArchType(control: UISegmentedControl) {
        switch control.selectedSegmentIndex {
        case 0:
            caller!.yolo.modelType = .spp
        case 1:
            caller!.yolo.modelType = .tiny
        default:
            return
        }
    }
    
    @objc func changeSaveErrors(control: UISwitch) {
        switch control.isOn {
        case false:
            caller!.saveErrors = false
        default:
            caller!.saveErrors = true
        }
    }
    
    @objc func changedEnableIdentification(control: UISwitch) {
        switch control.isOn {
        case false:
            caller!.enableIdentification = false
        default:
            caller!.enableIdentification = true
        }
    }
    
    @objc func changeDisplayExtraBoxes(control: UISwitch) {
        switch control.isOn {
        case false:
            caller!.displayExtraBoxes = false
        default:
            caller!.displayExtraBoxes = true
        }
    }
    
    @objc func changeYOLOConfidenceThreshold(control: UISlider) {
        let value = Float(round(control.value))
        caller!.yolo.confidenceThreshold = value / 100
        print(value / 100)
    }
    
    @objc func changeIdentificationConfidenceThreshold(control: UISlider) {
        let value = Float(round(control.value * 100) / 100.0)
        caller!.identificationThreshold = value / 100
        print(value / 100)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell()
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "yoloselection", for: indexPath)
                for view in cell.contentView.subviews {
                    if let segmentedControl = view as? UISegmentedControl {
                        segmentedControl.addTarget(self, action: #selector(self.changedArchType(control:)), for: .valueChanged)
                        switch caller?.yolo.modelType {
                        case .spp:
                            segmentedControl.selectedSegmentIndex = 0
                        default:
                            segmentedControl.selectedSegmentIndex = 1
                        }
                    }
                }
            } else if indexPath.row == 1 {
                let cell: ConfidenceThresholdTableViewCell = tableView.dequeueReusableCell(withIdentifier: "confidencethreshold", for: indexPath) as! ConfidenceThresholdTableViewCell
                cell.slider.minimumValue = 0
                cell.slider.maximumValue = 100
                cell.format2 = false
                cell.slider.setValue(caller!.yolo.confidenceThreshold * 100, animated: false)
                cell.slider.addTarget(cell, action: #selector(ConfidenceThresholdTableViewCell.changedValue(slider:)), for: .valueChanged)
                cell.slider.addTarget(self, action: #selector(self.changeYOLOConfidenceThreshold(control:)), for: .valueChanged)
                cell.changedValue(slider: cell.slider)
                return cell
            } else if indexPath.row == 2 {
                cell = tableView.dequeueReusableCell(withIdentifier: "displaybottle", for: indexPath)
                for view in cell.contentView.subviews {
                    if let control = view as? UISwitch {
                        control.addTarget(self, action: #selector(self.changeDisplayExtraBoxes(control:)), for: .valueChanged)
                        switch caller?.displayExtraBoxes {
                        case true:
                            control.isOn = true
                        default:
                            control.isOn = false
                        }
                    }
                }
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "enableidentification", for: indexPath)
                for view in cell.contentView.subviews {
                    if let control = view as? UISwitch {
                        control.addTarget(self, action: #selector(self.changedEnableIdentification(control:)), for: .valueChanged)
                        switch caller?.enableIdentification {
                        case true:
                            control.isOn = true
                        default:
                            control.isOn = false
                        }
                    }
                }
            } else if indexPath.row == 1 {
                let cell: ConfidenceThresholdTableViewCell = tableView.dequeueReusableCell(withIdentifier: "confidencethreshold", for: indexPath) as! ConfidenceThresholdTableViewCell
                cell.title.text = "Confidence Threshold"
                cell.slider.minimumValue = 90
                cell.slider.maximumValue = 100
                cell.slider.setValue(caller!.identificationThreshold * 100, animated: false)
                cell.slider.addTarget(cell, action: #selector(ConfidenceThresholdTableViewCell.changedValue(slider:)), for: .valueChanged)
                cell.slider.addTarget(self, action: #selector(self.changeIdentificationConfidenceThreshold(control:)), for: .valueChanged)
                cell.changedValue(slider: cell.slider)
                return cell
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "saveerrors", for: indexPath)
                for view in cell.contentView.subviews {
                    if let control = view as? UISwitch {
                        control.addTarget(self, action: #selector(self.changeSaveErrors(control:)), for: .valueChanged)
                        switch caller?.saveErrors {
                        case true:
                            control.isOn = true
                        default:
                            control.isOn = false
                        }
                    }
                }
            }
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = tableView.rowHeight
        if indexPath.section == 0 && indexPath.row == 1 {
            return 70
        }
        if indexPath.section == 1 && indexPath.row == 1 {
            return 70
        }
        return height
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Detection Settings"
        case 1:
            return "Identification Settings"
        case 2:
            return "Other"
        default:
            return "ERROR"
        }
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
