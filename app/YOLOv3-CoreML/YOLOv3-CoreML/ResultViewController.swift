//
//  ResultViewController.swift
//  Tavour-Single-Photo
//
//  Created by Vladislav Shakhray on 10.04.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var selectedImage: UIImage!
    var info: NSDictionary!

    private var bboxes: [[Float]]!
    private var classes: [String]!
    private var probs: [Double]!
    private var isIdentified: [Bool]!
    
    private var isLoaded = false
    
    private var xOffset: CGFloat!
    private var yOffset: CGFloat!
    
    func parseInfo() {
        bboxes = info["bboxes"] as! [[Float]]
        classes = info["classes"] as! [String]
        probs = info["probs"] as! [Double]
        isIdentified = info["is_identified"] as! [Bool]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
    
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(close))
        
        // Do any additional setup after loading the view.
    }
    
    @objc private func close() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func handleTap(gr: UITapGestureRecognizer) {
        let location = gr.location(in: resultImageView)
        let lx = Float(location.x)
        let ly = Float(location.y)
        for i in 0..<bboxes.count {
            let bbox = bboxes[i]
            if bbox[0] <= lx && lx <= bbox[2] && bbox[1] <= ly && ly <= bbox[3] {
                tableView.selectRow(at: IndexPath(row: i, section: 0), animated: true, scrollPosition: .middle)
                let time = DispatchTime.now() + .milliseconds(500)
                DispatchQueue.main.asyncAfter(deadline: time) {
                    self.tableView.deselectRow(at: IndexPath(row: i, section: 0), animated: true)
                }
                break
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !isLoaded {
            resultImageView!.image = selectedImage
                        
            for i in 0..<classes.count {
                let bbox = bboxes[i]
                
                let ratio = selectedImage.size.height / selectedImage.size.width
                let initialRatio = resultImageView.frame.height / resultImageView.frame.width
                
                if ratio > initialRatio {
                    yOffset = 0
                    
                    let resultingWidth = resultImageView.frame.height / ratio
                    xOffset = (resultImageView.frame.size.width - resultingWidth) / 2.0
                } else {
                    xOffset = 0
                    
                    let resultingHeight = resultImageView.frame.width * ratio
                    yOffset = (resultImageView.frame.size.height - resultingHeight) / 2.0
                }
                
                let scaleX = CGFloat((resultImageView.frame.size.width - 2 * xOffset) / selectedImage.size.width)
                let scaleY = CGFloat((resultImageView.frame.size.height - 2 * yOffset) / selectedImage.size.height)
                
                let x = CGFloat(bbox[0]) * scaleX + xOffset
                let y = CGFloat(bbox[1]) * scaleY + yOffset
                let w = CGFloat(bbox[2] - bbox[0]) * scaleX
                let h = CGFloat(bbox[3] - bbox[1]) * scaleY
                
                let view: UIView = UIView(frame: CGRect(x: x, y: y, width: w, height: h))
                view.layer.borderWidth = 1.5
                
                let color = isIdentified[i] ? UIColor(red: 0.0, green: 0.8, blue: 0.0, alpha: 1).cgColor:
                    UIColor(red: 0.9, green: 0.0, blue: 0.0, alpha: 1).cgColor
                view.layer.borderColor = color
                
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: 10, height:   10))
                label.text = String(i+1)
                
                var font: CGFloat = 14
                if bboxes.count > 10 {
                    font = 12
                }
                if bboxes.count > 20 {
                    font = 10
                }
                if bboxes.count > 30 {
                    font = 8
                }
                label.font = UIFont.systemFont(ofSize: font)
                label.sizeToFit()
                
                label.frame = CGRect(x: x + w - label.frame.width, y: y + h - label.frame.height, width: label.frame.width, height: label.frame.height)
                label.textColor = UIColor.white
                label.backgroundColor = UIColor(cgColor: color)
                
                bboxes[i] = [Float(x), Float(y), Float(x + w), Float(y + h)]

                resultImageView.addSubview(view)
                resultImageView.addSubview(label)
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(gr:)))
            resultImageView.addGestureRecognizer(tap)
            resultImageView.isUserInteractionEnabled = true
            
            isLoaded = true
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ResultViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bboxes.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56.0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "result", for: indexPath) as! ResultEntryCell
        cell.setIndex(indexPath.row + 1, withClass: classes[indexPath.row], probability: Float(probs[indexPath.row]))
        return cell
    }
}
