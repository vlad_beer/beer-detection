import UIKit
import LASwift
import Vision
import AVFoundation
import CoreMedia
import VideoToolbox

extension UIImage {
    public func padTo(width: CGFloat, height: CGFloat) -> UIImage {
        let resultingWidth = max(size.width, size.height * width / height)
        let resultingHeight = max(size.height, size.width * height / width)
        
        let horizontalOffset = (resultingWidth - size.width) / 2.0
        let verticalOffset = (resultingHeight - size.height) / 2.0
        
        let view: UIView = UIView(frame: CGRect(x: 0, y: 0, width: resultingWidth, height: resultingHeight))
        let imageView: UIImageView = UIImageView(frame: CGRect(x: horizontalOffset, y: verticalOffset, width: size.width, height: size.height))
        imageView.image = self
        view.addSubview(imageView)
        view.backgroundColor = UIColor.init(white: 0.5, alpha: 1.0)
        return image(with: view)!
    }
}

func scale(bbox: CGRect, toWidth width: CGFloat, toHeight height: CGFloat) -> CGRect {
    let scaleX = width / CGFloat(YOLO.inputWidth)
    let scaleY = height / CGFloat(YOLO.inputHeight)
    
    var rect = bbox
    
    rect.origin.x *= scaleX
    rect.origin.y *= scaleY
    rect.size.width *= scaleX
    rect.size.height *= scaleY
    
    return rect
}

func pad(boundingBox: CGRect, delta: CGFloat) -> CGRect {
    var rect = boundingBox
    rect.origin.x -= rect.width * delta
    rect.origin.y -= rect.height * delta
    rect.size.width *= 1 + 2 * delta
    rect.size.height *= 1 + 2 * delta
    
    return rect
}

func cropForIdentification(boundingBox: CGRect, image: UIImage) -> CVPixelBuffer? {
    let paddedBox = pad(boundingBox: boundingBox, delta: 0.1)
    let cg: CGImage? = image.cgImage
    if let cg_: CGImage = cg {
        let newImage = UIImage(cgImage: cg_.cropping(to:paddedBox)!)
        let paddedImage = DispatchQueue.main.sync {
            return newImage.padTo(width: CGFloat(Identifier.inputWidth), height: CGFloat(Identifier.inputHeight))
        }
        //            UIImageWriteToSavedPhotosAlbum(ne wImage, nil, nil, nil)
        let grayPixelBuffer = paddedImage.pixelBufferGray(width: Identifier.inputWidth, height: Identifier.inputHeight)!
        
        //            var debugImage: CGImage?
        //            VTCreateCGImageFromCVPixelBuffer(grayPixelBuffer, nil, &debugImage)
        //            UIImageWriteToSavedPhotosAlbum(UIImage(cgImage: debugImage!), nil, nil, nil)
        //
        return grayPixelBuffer
    } else {
        return nil
    }
}

func image(with view: UIView) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
    defer { UIGraphicsEndImageContext() }
    if let context = UIGraphicsGetCurrentContext() {
        view.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        return image
    }
    return nil
}

func get_memory() -> UInt{
    var taskInfo = mach_task_basic_info()
    var count = mach_msg_type_number_t(MemoryLayout<mach_task_basic_info>.size)/4
    let _: kern_return_t = withUnsafeMutablePointer(to: &taskInfo) {
        $0.withMemoryRebound(to: integer_t.self, capacity: 1) {
            task_info(mach_task_self_, task_flavor_t(MACH_TASK_BASIC_INFO), $0, &count)
        }
    }

    return UInt(taskInfo.resident_size) / 1000000
}

class ViewController: UIViewController {
    @IBOutlet weak var videoPreview: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var debugImageView: UIImageView!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var benchmarkButton: UIButton!

    public static let defaultIdentificationThreshold: Float = 0.996

    let yolo = YOLO()
    let identifier = Identifier()

    var enableIdentification: Bool = true
    var displayExtraBoxes: Bool = false
    var saveErrors: Bool = false
    
    var videoCapture: VideoCapture!
    var request: VNCoreMLRequest!
    var startTimes: [CFTimeInterval] = []
    
    var k: CGFloat = 0.0
    var width: CGFloat = 0.0
    var viewHeight: CGFloat = 0.0
    
    let settingsVC = SettingsViewController()
    
    var boundingBoxes = [BoundingBox]()
    var colors: [UIColor] = []
    
    let ciContext = CIContext()
    
    var isBenchmarking: Bool = false
    
    var resizedPixelBuffer: CVPixelBuffer?
    
    var image: UIImage = UIImage()
    var framesDone = 0
    var frameCapturingStartTime = CACurrentMediaTime()
    let semaphore = DispatchSemaphore(value: 2)
    
    var identificationThreshold: Float = ViewController.defaultIdentificationThreshold
    var lastTrackingPool: [ObjectTracker] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timeLabel.text = ""
        
        setUpBoundingBoxes()
        setUpCoreImage()
        setUpVision()
        setUpCamera()
        
        settingsButton.addTarget(self, action: #selector(pressedSettings), for: .touchUpInside)
        
        frameCapturingStartTime = CACurrentMediaTime()
        
        // Testing
//        let image = UIImage(named: "sample")
//        let padded = image?.padTo(width: CGFloat(Identifier.inputWidth), height: CGFloat(Identifier.inputHeight))
        
//        let pb = padded!.pixelBufferGray(width: Identifier.inputHeight, height: Identifier.inputHeight)!
//        var debugImage: CGImage?
//        VTCreateCGImageFromCVPixelBuffer(pb, options: nil, imageOut: &debugImage)
//        UIImageWriteToSavedPhotosAlbum(UIImage(cgImage: debugImage!), nil, nil, nil)
        
//        let embedding: Vector! = identifier.getEmbedding(pixelBuffer: pb)
    }
    
    @IBAction func benchmark(_ sender: Any) {
        DispatchQueue.main.async {
            self.isBenchmarking = true
            self.performSegue(withIdentifier: "benchmark", sender: self)

        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        k = 1000.0/750.0
        width = self.view.frame.width
        viewHeight = self.view.frame.height
    }
    
    @objc func pressedSettings() {
        performSegue(withIdentifier: "openSettings", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print(#function)
    }
    
    // MARK: - Initialization
    
    func setUpBoundingBoxes() {
        for _ in 0..<YOLO.maxBoundingBoxes {
            boundingBoxes.append(BoundingBox())
        }
        
        // Make colors for the bounding boxes. There is one color for each class,
        // 80 classes in total.
        for r: CGFloat in [0.2, 0.4, 0.6, 0.8] {
            for g: CGFloat in [0.3, 0.7, 0.6, 0.8] {
                for b: CGFloat in [0.4, 0.8, 0.6, 1.0] {
                    let color = UIColor(red: r, green: g, blue: b, alpha: 1)
                    colors.append(color)
                }
            }
        }
    }
    
    func setUpCoreImage() {
        let status = CVPixelBufferCreate(nil, YOLO.inputWidth, YOLO.inputHeight,
                                         kCVPixelFormatType_32BGRA, nil,
                                         &resizedPixelBuffer)
        
        if status != kCVReturnSuccess {
            print("Error: could not create resized pixel buffer", status)
        }
    }
    
    func setUpVision() {
        guard let visionModel = try? VNCoreMLModel(for: yolo.getMLModel()) else {
            print("Error: could not create Vision model")
            return
        }
        
        request = VNCoreMLRequest(model: visionModel, completionHandler: visionRequestDidComplete)
        
        // NOTE: If you choose another crop/scale option, then you must also
        // change how the BoundingBox objects get scaled when they are drawn.
        // Currently they assume the full input image is used.
        request.imageCropAndScaleOption = .scaleFill
    }
    
    func setUpCamera() {
        videoCapture = VideoCapture()
        videoCapture.delegate = self
        videoCapture.fps = 20
        videoCapture.setUp(sessionPreset: AVCaptureSession.Preset.photo) { success in
            if success {
                // Add the video preview into the UI.
                if let previewLayer = self.videoCapture.previewLayer {
                    self.videoPreview.layer.addSublayer(previewLayer)
                    self.resizePreviewLayer()
                }
                
                // Add the bounding box layers to the UI, on top of the video preview.
                for box in self.boundingBoxes {
                    box.addToLayer(self.videoPreview.layer)
                }
                
                // Once everything is set up, we can start capturing live video.
                self.videoCapture.start()
            }
        }
    }
    
    // MARK: - UI stuff
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        resizePreviewLayer()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    func resizePreviewLayer() {
        videoCapture.previewLayer?.frame = videoPreview.bounds
    }
    
    // MARK: - Doing inference
    func predict(image: UIImage) {
        if let pixelBuffer = image.pixelBuffer(width: YOLO.inputWidth, height: YOLO.inputHeight) {
            predict(pixelBuffer: pixelBuffer)
        }
    }
    
    func predict(pixelBuffer: CVPixelBuffer, pad: Bool = true) {
        if isBenchmarking {
            return
        }
        // Measure how long it takes to predict a single video frame.
        let startTime = CACurrentMediaTime()
        guard let resizedPixelBuffer = resizedPixelBuffer else { return }
        let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
        let sx = CGFloat(YOLO.inputWidth) / CGFloat(CVPixelBufferGetWidth(pixelBuffer))
        let sy = CGFloat(YOLO.inputHeight) / CGFloat(CVPixelBufferGetHeight(pixelBuffer))
        let scaleTransform = CGAffineTransform(scaleX: sx, y: sy)
        let scaledImage = ciImage.transformed(by: scaleTransform)
        ciContext.render(scaledImage, to: resizedPixelBuffer)
        let buffer = resizedPixelBuffer
        
//        var debugImage: CGImage?
//        VTCreateCGImageFromCVPixelBuffer(buffer, nil, &debugImage)
//        UIImageWriteToSavedPhotosAlbum(UIImage(cgImage: debugImage!), nil, nil, nil)
        
        if var boundingBoxes = try? yolo.predict(image: buffer) {
            
            // Filtering bounding boxes
            boundingBoxes = boundingBoxes.filter({ (prediction) -> Bool in
                let rect = prediction.rect
                if rect.height / rect.width > 3 {
                    return false
                }
                if rect.origin.x <= 0 || rect.origin.y <= 0
                    || rect.origin.x + rect.width >= CGFloat(YOLO.inputWidth)
                    || rect.origin.y + rect.height >= CGFloat(YOLO.inputHeight) {
                    return false
                }
                return true
            })

            // Adding smoothing
            var newPool: [ObjectTracker] = []
            for i in 0..<boundingBoxes.count {
                newPool.append(ObjectTracker(bbox: boundingBoxes[i].rect,
                                             classIndex: boundingBoxes[i].classIndex,
                                             score: boundingBoxes[i].score))
            }

            if lastTrackingPool.count > 0 {
                for pool in newPool {
                    var closestObj = lastTrackingPool[0]
                    var bestIOU = 0.0
                    for lastObj in lastTrackingPool {
                        if lastObj.classIndex != pool.classIndex {
                            continue
                        }
                        let distance = Double(pool.distanceTo(lastObj))
                        if distance >= bestIOU {
                            bestIOU = distance
                            closestObj = lastObj
                        }
                    }
                    
                    if bestIOU > 0.4 {
                        pool.track(bbox: closestObj.bboxHistory, score: pool.score)
                    }
                }
            }

            var embeddings: [Vector?] = Array(repeating: nil, count: newPool.count)
            var resultingBboxes: [CGRect] = []
            for i in 0..<newPool.count {
                let l = newPool[i]
                let boundingBox = l.resultingBox
                let scaledBoundingBox = scale(bbox: boundingBox!, toWidth: width, toHeight: width * k)
                resultingBboxes.append(scaledBoundingBox)
                if enableIdentification && [1, 2, 3].contains(boundingBoxes[i].classIndex) {
                    let cropScaled = scale(bbox: boundingBox!, toWidth: self.image.size.width, toHeight: self.image.size.height)
                    let grayPixelBuffer = cropForIdentification(boundingBox: cropScaled, image: self.image)!
                    embeddings[i] = identifier.getEmbedding(pixelBuffer: grayPixelBuffer)
                }
            }
            
            var resultingPredictions: [YOLO.Prediction] = []
            var resultingClasses: [Identifier.Prediction] = []
            for i in 0..<newPool.count {
                let obj = newPool[i]
//                if obj.bboxHistory.count < 3 {
//                    continue
//                }
                resultingPredictions.append(YOLO.Prediction(classIndex: obj.classIndex, score: obj.score, rect: resultingBboxes[i]))
                if let embedding = embeddings[i] {
                    let predictedClass = identifier.identify(embedding: embedding)
                    resultingClasses.append(predictedClass)
                    obj.logPrediction(pixelBuffer: pixelBuffer, cls: predictedClass.className, saveErrors: saveErrors)
                } else {
                    resultingClasses.append(Identifier.Prediction(className: labels[obj.classIndex], confidence: obj.score))
                }
            }
            let elapsed = CACurrentMediaTime() - startTime
            showOnMainThread(resultingPredictions, resultingClasses, elapsed)
            
            lastTrackingPool = newPool
        }
    }
    
    func predictUsingVision(pixelBuffer: CVPixelBuffer) {
        // Measure how long it takes to predict a single video frame. Note that
        // predict() can be called on the next frame while the previous one is
        // still being processed. Hence the need to queue up the start times.
        startTimes.append(CACurrentMediaTime())
        
        // Vision will automatically resize the input image.
        let handler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer)
        try? handler.perform([request])
    }
    
    func visionRequestDidComplete(request: VNRequest, error: Error?) {
        if let observations = request.results as? [VNCoreMLFeatureValueObservation],
            let features = observations.first?.featureValue.multiArrayValue {
            
            let boundingBoxes = yolo.computeBoundingBoxes(features: [features, features, features])
            let elapsed = CACurrentMediaTime() - startTimes.remove(at: 0)
            //            showOnMainThread(boundingBoxes, elapsed)
        }
    }
    
    func showOnMainThread(_ boundingBoxes: [YOLO.Prediction], _ classes: [Identifier.Prediction], _ elapsed: CFTimeInterval) {
        DispatchQueue.main.async {
            self.show(predictions: boundingBoxes, classes: classes)
            
            let fps = 1.0 / elapsed
            
            self.timeLabel.text = String(format: "FPS: %.2f\tMEM: %dMB", fps, get_memory())
            self.semaphore.signal()
        }
    }
    
    func show(predictions: [YOLO.Prediction], classes: [Identifier.Prediction]) {
        for i in 0..<boundingBoxes.count {
            boundingBoxes[i].hide()
        }
        
        for i in 0..<predictions.count {
            let prediction = predictions[i]
            
            let showLabel: Bool = [1, 2, 3].contains(prediction.classIndex)
            var showBbox: Bool = displayExtraBoxes
            if [1, 2, 3].contains(prediction.classIndex) {
                showBbox = true
            }
            
            if !showBbox {
                continue
            }
            
            var rect = prediction.rect
            rect.origin.y += (view.bounds.height - k * view.bounds.width) / 2.0
            
            // Show the bounding box.
            var color: UIColor
            if enableIdentification {
                if classes[i].confidence > self.identificationThreshold {
                    color = UIColor(red: 0.1, green: 0.8, blue: 0.05, alpha: 1.0)
                } else {
                    color = UIColor(red: 0.8, green: 0.1, blue: 0.05, alpha: 1.0)
                }
            } else {
                color = colors[prediction.classIndex]
            }
            let confidence = String(format: "%.2f", classes[i].confidence)
            boundingBoxes[i].show(frame: rect, label: " " + classes[i].className, confidence: " " + confidence, color: color, drawLabel: showLabel)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let id = segue.identifier!
        if id == "openSettings" {
            let destNav: UINavigationController = segue.destination as! UINavigationController
            let dest: SettingsViewController = destNav.topViewController as! SettingsViewController
            dest.caller = self
        } else {
            let nvc = segue.destination as! UINavigationController
            let vc = nvc.topViewController as! SpeedBenchmarkingViewController
            nvc.modalPresentationStyle = .fullScreen
            vc.caller = self
        }
    }
}

extension ViewController: VideoCaptureDelegate {
    func videoCapture(_ capture: VideoCapture, didCaptureVideoFrame pixelBuffer: CVPixelBuffer?, timestamp: CMTime) {
        // For debugging.
        //predict(image: UIImage(named: "dog416")!); return
        
        semaphore.wait()
        
        if let pixelBuffer = pixelBuffer {
            // For better throughput, perform the prediction on a background queue
            // instead of on the VideoCapture queue. We use the semaphore to block
            // the capture queue and drop frames when Core ML can't keep up.
            var debugImage: CGImage?
            VTCreateCGImageFromCVPixelBuffer(pixelBuffer, options: nil, imageOut: &debugImage)
            self.image = UIImage(cgImage: debugImage!)
//            print(self.image.size)
            //            UIImageWriteToSavedPhotosAlbum(self.image, nil, nil, nil)
            
            DispatchQueue.global().async {
                self.predict(pixelBuffer: pixelBuffer)
                //                self.predictUsingVision(pixelBuffer: pixelBuffer)
            }
        }
    }
}
