//
//  Identifier.swift
//  YOLOv3-CoreML
//
//  Created by Vladislav Shakhray on 04.03.2020.
//  Copyright © 2020 MachineThink. All rights reserved.
//

import Foundation
import UIKit
import CoreML
import LASwift

class Identifier {
    
    let model = identifier_v3_256()
    let head = head_v3_256()
    
    let topK = 25

    struct Prediction {
        let className: String
        let confidence: Float
    }

    public static let inputWidth = 256
    public static let inputHeight = 256
    public static let featureDum = 512
    
    private var classes: [String] = []
    private var index: Matrix? = nil
    private var index_tsvd: Matrix? = nil
    private var tsvd_mat: Matrix? = nil
    
    private func readClasses() {
        if let filepath = Bundle.main.path(forResource: "classes_v3", ofType: "txt") {
            do {
                let contents: String = try String(contentsOfFile: filepath)
                contents.enumerateLines { (line, _) -> () in
                    self.classes.append(line)
                }

                assert(classes.count == 2431)
                print("Loaded classes: " + String(classes.count))
            } catch {
                print("contents could not be loaded")
            }
        } else { }
    }

    private func readMat(filename: String) -> Matrix? {
        if let filepath = Bundle.main.path(forResource: filename, ofType: "txt") {
            do {
                var lines: [String] = []
                let contents: String = try String(contentsOfFile: filepath)
                contents.enumerateLines { (line, _) -> () in
                    lines.append(line)
                }
                
                var vectors: [[Double]] = []
                var i = 0
                for line in lines {
                    var vec: [Double] = []
                    for seq in line.components(separatedBy: " ") {
                        vec.append(Double(seq)!)
                    }
                    vectors.append(vec)
                    i += 1
                }
                
                let matrix = Matrix(vectors)
                print("Matrix \(matrix.rows)x\(matrix.cols) loaded from \"\(filename).txt\".")
                return matrix
            } catch {
                print("Error info: \(error)")
                return nil
            }
        } else {
            return nil
        }
    }
    
    public init() {
        readClasses()
        index = readMat(filename: "vectors_v3")
        index_tsvd = readMat(filename: "vectors_v3_tsvd_16")
        tsvd_mat = readMat(filename: "tsvd_mat_v3")
    }
    
    private func vec2ml(vec: Vector) -> MLMultiArray {
        let mlArray = try! MLMultiArray(shape: [NSNumber(value: vec.count)], dataType: MLMultiArrayDataType.float32)
        for i in 0..<Identifier.featureDum {
             mlArray[i] = NSNumber(value: vec[i])
        }
        return mlArray
    }
    
    public func identify(embedding: Vector) -> Prediction {
        // Applying TruncatedSVD to vector
        let embeddingMatrix: Matrix = Matrix([embedding])
        let embeddingTSVD = (embeddingMatrix * tsvd_mat!)[row: 0]
        
        // Get L2 distances
        let l2 = sum(power(self.index_tsvd! - embeddingTSVD, 2), .Row)
        typealias Entry = (Double, Int)
        var pairs: [Entry] = Array(repeating: (0.0, 0), count: l2.count)
        for i in 0..<l2.count { // Zipping
            pairs[i] = (l2[i], i)
        }
        
        // Getting topK elements
        var bestL2: Double = 999999.0
        var bestIndex: Int = -1
        var heap = Heap(array: pairs) { (pairA: Entry, pairB: Entry) -> Bool in
            return pairA.0 < pairB.0
        }
        for _ in 0..<topK {
            let entry = heap.remove()!
            let cl2 = sum(power((embedding - self.index![row: entry.1]), 2))
            if cl2 < bestL2 {
                bestIndex = entry.1
                bestL2 = cl2
            }
        }
        
        // Getting the class name
        var className: String = ""
        let comps = self.classes[bestIndex].components(separatedBy: " - ")
        if comps[0].count <= 5 {
            className += comps[0] + " - "
        } else {
            className += comps[0].prefix(5) + ". - "
        }
        className += comps[1]
        
        // Calculating the probability
        let trainFeature = vec2ml(vec: self.index![row: bestIndex])
        let embeddingFeature = vec2ml(vec: embedding)
        
        let probability = try! head.prediction(features1: trainFeature, features2: embeddingFeature).probability[0].floatValue
        return Prediction(className: className, confidence: probability)
    }
    
    public func getEmbedding(pixelBuffer: CVPixelBuffer) -> Vector? {
        do {
            let output = try model.prediction(image: pixelBuffer)
            let features = output.output
            var embedding: [Double] = Array(repeating: 0, count: features.count)
            for i in 0..<features.count {
                embedding[i] = Double(features[i].floatValue)
            }
            return embedding
        } catch {
            return nil
        }
    }
}
