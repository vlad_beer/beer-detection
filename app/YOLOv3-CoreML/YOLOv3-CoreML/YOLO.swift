import Foundation
import UIKit
import CoreML

enum YOLOv3Type {
    case spp
    case tiny
}

class YOLO {
    public static let defaultConfidenceThreshold: Float = 0.2

    public static var inputWidth = 384
    public static var inputHeight = 512
    public static let maxBoundingBoxes = 1000
    
    public var confidenceThreshold: Float = YOLO.defaultConfidenceThreshold
    let iouThreshold: Float = 0.2
    
    struct Prediction {
        let classIndex: Int
        let score: Float
        let rect: CGRect
    }
    
    let model_YOLOv3SPP = yolov3_spp_384_float()
    let model_tinyYOLOv3 = tiny_yolov3_v2_384_float()
    
    var modelType: YOLOv3Type = .spp
    
    public init() {
    }

    public func predict(image: CVPixelBuffer) -> [Prediction] {
        if modelType == .spp {
            if let output = try? model_YOLOv3SPP.prediction(image: image) {
                let boundingBoxes = computeBoundingBoxes(features: [output.output1, output.output2, output.output3])
                return boundingBoxes
            } else {
                return []
            }
        } else if modelType == .tiny {
            if let output = try? model_tinyYOLOv3.prediction(image: image) {
                let boundingBoxes = computeBoundingBoxes(features: [output.output1, output.output2])
                return boundingBoxes
            } else {
                return []
            }
        }
        return []
    }
    
    public func getMLModel() -> MLModel {
        switch modelType {
        case .spp:
            return model_YOLOv3SPP.model
        default:
            return model_tinyYOLOv3.model
        }
    }
    
    public func computeBoundingBoxes(features: [MLMultiArray]) -> [Prediction] {
        var predictions = [Prediction]()
        
        let boxesPerCell = 3
        let numClasses = 6
        
        // 384x512
        let gridHeight = [16, 32, 64]
        let gridWidth = [12, 24, 48]
        
        var featurePointer = UnsafeMutablePointer<Float>(OpaquePointer(features[0].dataPointer))
        var channelStride = features[0].strides[0].intValue
        var yStride = features[0].strides[1].intValue
        var xStride = features[0].strides[2].intValue
        
        func offset(_ channel: Int, _ x: Int, _ y: Int) -> Int {
            return channel*channelStride + y*yStride + x*xStride
        }
        
        for i in 0..<features.count {
            
            let stride = Float(max(YOLO.inputWidth, YOLO.inputHeight) / max(gridWidth[i], gridHeight[i]))
            
            featurePointer = UnsafeMutablePointer<Float>(OpaquePointer(features[i].dataPointer))
            channelStride = features[i].strides[0].intValue
            yStride = features[i].strides[1].intValue
            xStride = features[i].strides[2].intValue
            for cy in 0..<gridHeight[i] {
                for cx in 0..<gridWidth[i] {
                    for b in 0..<boxesPerCell {
                        // For the first bounding box (b=0) we have to read channels 0-24,
                        // for b=1 we have to read channels 25-49, and so on.
                        let channel = b*(numClasses + 5)
                        
                        // The fast way:
                        let tx = Float(featurePointer[offset(channel    , cx, cy)])
                        let ty = Float(featurePointer[offset(channel + 1, cx, cy)])
                        let tw = Float(featurePointer[offset(channel + 2, cx, cy)])
                        let th = Float(featurePointer[offset(channel + 3, cx, cy)])
                        let tc = Float(featurePointer[offset(channel + 4, cx, cy)])
                        
                        // The predicted tx and ty coordinates are relative to the location
                        // of the grid cell; we use the logistic sigmoid to constrain these
                        // coordinates to the range 0 - 1. Then we add the cell coordinates
                        // (0-12) and multiply by the number of pixels per grid cell (32).
                        // Now x and y represent center of the bounding box in the original
                        // 416x416 image space.

                        let x = (Float(cx) + sigmoid(tx)) * stride
                        let y = (Float(cy) + sigmoid(ty)) * stride
                        
                        // The size of the bounding box, tw and th, is predicted relative to
                        // the size of an "anchor" box. Here we also transform the width and
                        // height into the original 416x416 image space.
                        let w = exp(tw) * anchors[i][2*b    ]
                        let h = exp(th) * anchors[i][2*b + 1]
                        
                        // The confidence value for the bounding box is given by tc. We use
                        // the logistic sigmoid to turn this into a percentage.
                        let confidence = sigmoid(tc)
                        
                        // Gather the predicted classes for this anchor box and softmax them,
                        // so we can interpret these numbers as percentages.
                        var classes = [Float](repeating: 0, count: numClasses)
                        for c in 0..<numClasses {
                            // The slow way:
                            //classes[c] = features[[channel + 5 + c, cy, cx] as [NSNumber]].floatValue
                            
                            // The fast way:
                            classes[c] = Float(featurePointer[offset(channel + 5 + c, cx, cy)])
                        }
                        classes = softmax(classes)
                        
                        // Find the index of the class with the largest score.
                        let (detectedClass, bestClassScore) = classes.argmax()
                        
                        // Combine the confidence score for the bounding box, which tells us
                        // how likely it is that there is an object in this box (but not what
                        // kind of object it is), with the largest class prediction, which
                        // tells us what kind of object it detected (but not where).
                        let confidenceInClass = bestClassScore * confidence
                        
                        // Since we compute 13x13x3 = 507 bounding boxes, we only want to
                        // keep the ones whose combined score is over a certain threshold.
                        if confidenceInClass > confidenceThreshold {
                            let rect = CGRect(x: CGFloat(x - w/2), y: CGFloat(y - h/2),
                                              width: CGFloat(w), height: CGFloat(h))
                            
                            let prediction = Prediction(classIndex: detectedClass,
                                                        score: confidenceInClass,
                                                        rect: rect)
                            predictions.append(prediction)
                        }
                    }
                }
            }
        }
        
        // We already filtered out any bounding boxes that have very low scores,
        // but there still may be boxes that overlap too much with others. We'll
        // use "non-maximum suppression" to prune those duplicate bounding boxes.
        return nonMaxSuppression(boxes: predictions, limit: YOLO.maxBoundingBoxes, threshold: iouThreshold)
    }
}
