//
//  ViewController.swift
//  Tavour-Single-Photo
//
//  Created by Vladislav Shakhray on 10.04.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit
import AFNetworking
import VideoToolbox

func getImage(with view: UIView) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
    defer { UIGraphicsEndImageContext() }
    if let context = UIGraphicsGetCurrentContext() {
        view.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        return image
    }
    return nil
}

class SingleFrameViewController: UIViewController,  UINavigationControllerDelegate {
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var photoLibraryButton: UIButton!
    
    var imagePicker: UIImagePickerController!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var selectedImage: UIImage!
    var processedImage: UIImage!
    
    var response: NSDictionary!

    private let yolo = YOLO()
    private let identifier = Identifier()
    
    @objc func takePhoto() {
        imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        present(imagePicker, animated: true)
    }
    
    @IBAction func loadFromPhotoLibrary(_ sender: Any) {
        imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cameraButton.addTarget(self, action: #selector(takePhoto), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showResult" {
            let navigationController = segue.destination as! UINavigationController
            let resultViewController = navigationController.topViewController as! ResultViewController
            resultViewController.selectedImage = self.selectedImage
            resultViewController.info = self.response
            resultViewController.parseInfo()
        }
    }
    
    private func applyBoundingBoxes(image: UIImage, bboxes: [YOLO.Prediction]) -> UIImage? {
        let width = image.size.width
        let height = image.size.height
        
        let scaleX: Float = Float(width) / Float(YOLO.inputWidth)
        let scaleY: Float = Float(height) / Float(YOLO.inputHeight)
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        imageView.image = image
        
        for bbox in bboxes {
            if ![1, 2, 3].contains(bbox.classIndex) {
                continue
            }
            var rect = bbox.rect
            rect.origin.x *= CGFloat(scaleX)
            rect.size.width *= CGFloat(scaleX)
            rect.origin.y *= CGFloat(scaleY)
            rect.size.height *= CGFloat(scaleY)
            
            let view = UIView(frame: rect)
            view.layer.borderColor = UIColor.blue.cgColor
            view.layer.borderWidth = 6
            imageView.addSubview(view)
        }
        
        return getImage(with: imageView)
    }
    
    func sendPhoto(image: UIImage) {
        let imageData = image.jpegData(compressionQuality: 0.4)!
        
        let compressedImage = UIImage.init(data: imageData)!
//
//        let pb = compressedImage.pixelBuffer(width: YOLO.inputWidth, height: YOLO.inputHeight)
//
//        let predictions = yolo.predict(image: pb!)
//
//        var results: [(String, Double, Bool, [Float])] = []
//
//        // Getting identification results
//        for prediction in predictions {
//            let rect = scale(bbox: prediction.rect, toWidth: compressedImage.size.width, toHeight: compressedImage.size.height)
//            print(rect)
//            let paddedBox = pad(boundingBox: rect, delta: 0.1)
//            print(paddedBox)
//            let cg = compressedImage.cgImage!
//            let newImage = UIImage(cgImage: cg.cropping(to:paddedBox)!)
//            let paddedImage = newImage.padTo(width: CGFloat(Identifier.inputWidth), height: CGFloat(Identifier.inputHeight))
//            let grayPixelBuffer = paddedImage.pixelBufferGray(width: Identifier.inputWidth, height: Identifier.inputHeight)!
//
//            var debugImage: CGImage?
//            VTCreateCGImageFromCVPixelBuffer(grayPixelBuffer, options: nil, imageOut: &debugImage)
//            UIImageWriteToSavedPhotosAlbum(UIImage(cgImage: debugImage!), nil, nil, nil)
//            let embedding = identifier.getEmbedding(pixelBuffer: grayPixelBuffer)!
//            let identificationResult = identifier.identify(embedding: embedding)
//            results.append((identificationResult.className, Double(identificationResult.confidence), identificationResult.confidence > 0.99, [Float(rect.origin.x), Float(rect.origin.y), Float(rect.origin.x + rect.width), Float(rect.origin.y + rect.height)]))
//        }
//
//
//        let resultingYOLOimage = applyBoundingBoxes(image: compressedImage, bboxes: predictions)!
//        self.processedImage = resultingYOLOimage

        let request = AFHTTPRequestSerializer().multipartFormRequest(withMethod: "POST", urlString: "http://35.247.124.68:5560/mobile_upload", parameters: nil, constructingBodyWith: { (multipartFormData) in
            multipartFormData.appendPart(withFileData: imageData, name: "image", fileName: "image.jpeg", mimeType: "image/jpeg")
        }, error: nil)
        
        let manager = AFURLSessionManager(sessionConfiguration: .default)
        manager.responseSerializer = AFJSONResponseSerializer()
    
        let uploadTask: URLSessionUploadTask = manager.uploadTask(withStreamedRequest: request as! URLRequest, progress: { (progress) in
            
        }) { (response, responseObject, error) in
            self.response = responseObject as! NSDictionary
            print(self.response)
            self.performSegue(withIdentifier: "showResult", sender: self)
            self.activityIndicator.isHidden = true
        }
        
        uploadTask.resume()
    }
}

extension SingleFrameViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        activityIndicator.isHidden = false
        selectedImage = (info[.originalImage] as! UIImage)
        sendPhoto(image: self.selectedImage)
        imagePicker.dismiss(animated: true)
    }
}
