//
//  ResultEntryCell.swift
//  YOLOv3-CoreML
//
//  Created by Vladislav Shakhray on 16.04.2020.
//  Copyright © 2020 MachineThink. All rights reserved.
//

import UIKit

class ResultEntryCell: UITableViewCell {
    @IBOutlet weak var indexLabel: UILabel!
    @IBOutlet weak var breweryLabel: UILabel!
    @IBOutlet weak var beerLabel: UILabel!
    @IBOutlet weak var probabilityLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setIndex(_ index: Int, withClass cls: String, probability: Float) {
        indexLabel.text = String(format: "%d.", index)
        let comps = cls.components(separatedBy: " - ")
        breweryLabel.text = comps[0]
        beerLabel.text = comps[1]
        probabilityLabel.text = String(format: "%.3f", probability)
    }
}
