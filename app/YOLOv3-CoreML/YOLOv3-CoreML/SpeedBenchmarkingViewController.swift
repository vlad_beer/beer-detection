//
//  SpeedBenchmarkingViewController.swift
//  YOLOv3-CoreML
//
//  Created by Vladislav Shakhray on 02.04.2020.
//  Copyright © 2020 MachineThink. All rights reserved.
//

import UIKit

extension Float {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}

class SpeedBenchmarkingViewController: UIViewController {

    @IBOutlet weak var activity: UIActivityIndicatorView!
    var caller: ViewController? = nil
    @IBOutlet weak var progress: UIProgressView!
    
    let idCount = 100
    let yoloCount = 30

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.activity.startAnimating()
        }
        
        
        startBenchmarks()
    }
    
    func done() {
        dismiss(animated: true) {
            self.caller!.isBenchmarking = false
            self.caller!.semaphore.signal()
        }
    }
    
    private func startBenchmarks() {
        let cg: CGImage = caller!.image.cgImage!
        let newImage = UIImage(cgImage: cg.cropping(to:CGRect(x: 0, y: 0, width: 100, height: 100))!)
        let grayPixelBuffer = newImage.pixelBufferGray(width: Identifier.inputWidth, height: Identifier.inputHeight)!
        
        DispatchQueue.global().async {
            
            let yoloStartTime = CFAbsoluteTimeGetCurrent()
            for i in 0..<self.yoloCount {
                print("yolo \(i)")
                do {
                    try self.caller!.yolo.predict(image: self.caller!.image.pixelBuffer(width: YOLO.inputWidth, height: YOLO.inputHeight)!)
                } catch {
                    
                }
                DispatchQueue.main.async {
                    self.progress.setProgress(Float(i) / Float(self.yoloCount) / 2.0, animated: true)
                }
            }
            let yoloEndTime = CFAbsoluteTimeGetCurrent()
            
            let startTime = CFAbsoluteTimeGetCurrent()
            for i in 0..<self.idCount {
                print("iden \(i)")
                let emb = self.caller!.identifier.getEmbedding(pixelBuffer: grayPixelBuffer)
                self.caller!.identifier.identify(embedding: emb!)
                DispatchQueue.main.async {
                    self.progress.setProgress(0.5 + Float(i) / Float(2 * self.idCount), animated: true)
                }
                
            }
            let endTime = CFAbsoluteTimeGetCurrent()
            
            DispatchQueue.main.async {
                let yps = Float((yoloEndTime - yoloStartTime) / Double(self.yoloCount))
                let ps = Float((endTime - startTime) / Double(self.idCount))
                let alert = UIAlertController(title: "Benchmark Results", message: "YOLO:\nPer image: \(yps.rounded(toPlaces: 4))s\n10 images:\((yps * 10).rounded(toPlaces: 2))s\n\nIdentification:\nPer image: \(ps.rounded(toPlaces: 4))s.\n10 images: \((ps * 10).rounded(toPlaces: 2))s.\n20 images: \((ps * 20).rounded(toPlaces: 2))s.\n50 images: \((ps * 50).rounded(toPlaces: 2))s.", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "Done", style: .cancel, handler: { action in
                    self.done()
                }))
                
                alert.addAction(UIAlertAction(title: "Repeat", style: .default, handler: { action in
                    self.startBenchmarks()
                }))

                self.present(alert, animated: true)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
