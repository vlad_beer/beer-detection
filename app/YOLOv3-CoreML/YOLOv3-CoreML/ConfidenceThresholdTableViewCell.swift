//
//  ConfidenceThresholdTableViewCell.swift
//  YOLOv3-CoreML
//
//  Created by Vladislav Shakhray on 26.03.2020.
//  Copyright © 2020 MachineThink. All rights reserved.
//

import Foundation
import UIKit

class ConfidenceThresholdTableViewCell: UITableViewCell {
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var title: UILabel!
    
    var format2: Bool = true
    @objc func changedValue(slider: UISlider) {
        let value = Double(round(slider.value * 100) / 100.0)
        if format2 {
            confidenceLabel.text = String(format: "%.2f%%", value)
        } else {
            confidenceLabel.text = String(format: "%.0f%%", value)
        }
    }
}
