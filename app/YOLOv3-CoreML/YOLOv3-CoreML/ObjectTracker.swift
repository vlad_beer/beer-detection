//
//  ObjectTracker.swift
//  YOLOv3-CoreML
//
//  Created by Vladislav Shakhray on 18.03.2020.
//  Copyright © 2020 MachineThink. All rights reserved.
//

import Foundation
import UIKit
import CoreML
import LASwift

class ObjectTracker {
    public static let maxFrames = 5
    
    var bboxHistory: [CGRect] = []
    
    var lastPixelBuffer: CVPixelBuffer? = nil
    var lastClass: String? = nil
    
    var resultingBox: CGRect?
    
    let classIndex: Int
    var score: Float
    
    func logPrediction(pixelBuffer: CVPixelBuffer, cls: String, saveErrors: Bool) {
        if let pb = lastPixelBuffer {
            if saveErrors && lastClass != cls {
                savePixelBuffer(pixelBuffer: lastPixelBuffer!)
                savePixelBuffer(pixelBuffer: pixelBuffer)
            }
        }
        lastPixelBuffer = pixelBuffer
        lastClass = cls
    }
    
    init(bbox: CGRect, classIndex: Int, score: Float) {
        bboxHistory = [bbox]
        self.classIndex = classIndex
        self.score = score
        
        calculate()
    }
    
    func track(bbox: [CGRect], score: Float) {
        self.score = score
        bboxHistory += bbox
        if bboxHistory.count > ObjectTracker.maxFrames {
            bboxHistory.removeLast(bboxHistory.count - ObjectTracker.maxFrames)
        }
        
        calculate()
    }
    
    func distanceTo(_ tracker: ObjectTracker) -> Float {
        return IOU(a: resultingBox!, b: tracker.resultingBox!)
    }
    
    func calculate() {
        resultingBox = CGRect(x: 0, y: 0, width: 0, height: 0)
        for bbox in bboxHistory {
            resultingBox!.origin.x += bbox.origin.x
            resultingBox!.origin.y += bbox.origin.y
            resultingBox!.size.width += bbox.size.width
            resultingBox!.size.height += bbox.size.height
        }
        let count = bboxHistory.count
        resultingBox!.origin.x /= CGFloat(count)
        resultingBox!.origin.y /= CGFloat(count)
        resultingBox!.size.width /= CGFloat(count)
        resultingBox!.size.height /= CGFloat(count)
    }
}
