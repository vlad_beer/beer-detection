import Foundation
import UIKit

class BoundingBox {
    let shapeLayer: CAShapeLayer
    let textLayer: CATextLayer
    let confidenceLayer: CATextLayer
    
    init() {
        shapeLayer = CAShapeLayer()
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 3
        shapeLayer.isHidden = true
        
        textLayer = CATextLayer()
        textLayer.foregroundColor = UIColor.white.cgColor
        textLayer.isHidden = true
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.fontSize = 14
        textLayer.isWrapped = true
        textLayer.font = UIFont(name: "Avenir", size: textLayer.fontSize)

//        textLayer.alignmentMode = kCAAlignmentCenter
        
        confidenceLayer = CATextLayer()
        confidenceLayer.foregroundColor = UIColor.white.cgColor
        confidenceLayer.isHidden = true
        confidenceLayer.contentsScale = UIScreen.main.scale
        confidenceLayer.fontSize = 14
        confidenceLayer.font = UIFont(name: "Avenir", size: textLayer.fontSize)
//        confidenceLayer.alignmentMode = kCAAlignmentCenter
    }
    
    func addToLayer(_ parent: CALayer) {
        parent.addSublayer(shapeLayer)
        parent.addSublayer(textLayer)
        parent.addSublayer(confidenceLayer)
    }
    
    func show(frame: CGRect, label: String, confidence: String, color: UIColor, drawLabel: Bool) {
        CATransaction.setDisableActions(true)
        
        let path = UIBezierPath(roundedRect: frame, byRoundingCorners: [.bottomRight, .bottomLeft], cornerRadii: CGSize(width: 4, height: 4))
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.isHidden = false
        
        confidenceLayer.isHidden = true
        textLayer.isHidden = true
        
        if drawLabel {
            // Setting up confidence label
            confidenceLayer.string = confidence
            confidenceLayer.backgroundColor = color.cgColor
            
            let attributesC = [
                NSAttributedString.Key.font: confidenceLayer.font as Any
            ]
            
            let textRectC = confidence.boundingRect(with: CGSize(width: frame.width, height: 200),
                                                    options: .truncatesLastVisibleLine,
                                              attributes: attributesC, context: nil)
            let textSizeC = CGSize(width: textRectC.width + 12, height: textRectC.height)
            let textOriginC = CGPoint(x: frame.origin.x - 2, y: frame.origin.y)
            confidenceLayer.frame = CGRect(origin: textOriginC, size: textSizeC)
            
            var maskPath: UIBezierPath = UIBezierPath(roundedRect: confidenceLayer.bounds, byRoundingCorners: [.bottomRight], cornerRadii: CGSize(width: 6, height: 6))

            // Create the shape layer and set its path
            var  maskLayer = CAShapeLayer()
            maskLayer.frame = confidenceLayer.bounds;
            maskLayer.path = maskPath.cgPath;

            // Set the newly created shape layer as the mask for the image view's layer
            confidenceLayer.mask = maskLayer;
            
            // Setting up text
            textLayer.string = label
            textLayer.backgroundColor = color.cgColor

            let attributes = [
                NSAttributedString.Key.font: textLayer.font as Any
            ]
            
            let textRect = label.boundingRect(with: CGSize(width: frame.width, height: 2 * textSizeC.height),
                                              options: .truncatesLastVisibleLine,
                                              attributes: attributes, context: nil)
            let textSize = CGSize(width: frame.width + 4, height: textRect.height)
            let textOrigin = CGPoint(x: frame.origin.x - 2, y: frame.origin.y - textSize.height)
            textLayer.frame = CGRect(origin: textOrigin, size: textSize)
            
            maskPath = UIBezierPath(roundedRect: textLayer.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 6, height: 6))

            // Create the shape layer and set its path
            maskLayer = CAShapeLayer()
            maskLayer.frame = textLayer.bounds;
            maskLayer.path = maskPath.cgPath;

            // Set the newly created shape layer as the mask for the image view's layer
            textLayer.mask = maskLayer;
            
            textLayer.isHidden = false
            confidenceLayer.isHidden = false
        }
    }
    
    func hide() {
        shapeLayer.isHidden = true
        textLayer.isHidden = true
        confidenceLayer.isHidden = true
    }
}
