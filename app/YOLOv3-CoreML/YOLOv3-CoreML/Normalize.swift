import Foundation
import CoreML
import Accelerate

@objc(Normalize) class Normalize: NSObject, MLCustomLayer {
    
    private var index = 0
    
    required init(parameters: [String : Any]) throws {
        
        super.init()
    }
    
    func setWeightData(_ weights: [Data]) throws {
    }
    
    func outputShapes(forInputShapes inputShapes: [[NSNumber]]) throws
        -> [[NSNumber]] {
            return inputShapes
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func saveMultiArray(array: MLMultiArray) {
        var str = ""
        for i in 0..<array.count {
            str += String(array[i].floatValue) + " "
        }
        
        let filename = getDocumentsDirectory().appendingPathComponent("input_" + String(index) + ".txt")

        index += 1
    
        do {
            try str.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
            print("Written!")
        } catch {
            print("Something bad :(")
            // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
        }
    }
    
    func evaluate(inputs: [MLMultiArray], outputs: [MLMultiArray]) throws {
        for i in 0..<inputs.count {
            let input = inputs[i]
            let output = outputs[i]
            
            assert(input.dataType == .float32)
            assert(output.dataType == .float32)
            assert(input.shape == output.shape)
            
            let iptr = UnsafeMutablePointer<Float>(OpaquePointer(input.dataPointer))
            let optr = UnsafeMutablePointer<Float>(OpaquePointer(output.dataPointer))
            
            // Allocating constants
            let n = vDSP_Length(input.count)
            let s = vDSP_Length(192)
            let stride = vDSP_Stride(1)
            
            // Normalization (Swift.Accelerate)
            var mean: Float = .nan
            var std: Float = .nan
            vDSP_normalize(iptr, stride, optr, stride, &mean, &std, n)
            
            // Restructuring
//            vDSP_mtrans(iptr, stride, optr, stride, s, s)
//            for i in 0..<s {
//                vDSP_vrvrs(&optr[Int(192 * i)], stride, s)
//            }
        }
    }
}
