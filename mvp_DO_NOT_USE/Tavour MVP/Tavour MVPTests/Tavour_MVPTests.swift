//
//  Beer_MVPTests.swift
//  Tavour MVPTests
//
//  Created by Vladislav Shakhray on 17.05.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//


import XCTest
@testable import Tavour_MVP

class Beer_MVPTests: XCTestCase {

    var identifier: Identifier!
    var yolo: YOLO!

    override func setUp() {
        super.setUp()
        identifier = Identifier()
        yolo = YOLO()
    }

    override func tearDown() {
        identifier = nil
        yolo = nil
        super.tearDown()
    }

    func testIdentifier() throws {
        // Download an image of a Guinness beer,
        // assert that the identifier successfully identifies it as Guinness
        let urlString = "https://products2.imgix.drizly.com/ci-guinness-draught-420c95ffc7f4bdc0.jpeg"
        
        guard let url = URL(string: urlString) else { return }
        guard let data = try? Data(contentsOf: url) else { return }
        let image = UIImage(data: data)
        
        measure {
            guard let predictions = identifier.predict(image: (image?.pixelBuffer(width: Identifier.inputWidth, height: Identifier.inputHeight))!, topK: 3) else {
                XCTFail("Identifier predictions are nil.")
                return
            }
            
            XCTAssert(predictions.count == 3, "Invalid predictions count.")
            XCTAssert(predictions[0].className.contains("Guinness"), "Guinness beer not identified.")
        }
    }

    func testYOLO() throws {
        // Download an image containing a beer can, assert that YOLO detects it
        let urlString = "https://cdn11.bigcommerce.com/s-9w4zwzbhcc/images/stencil/2000x2000/products/1141/3104/Miller_Lite_Beer_Can_Security_Container_12oz__34083.1551980199.JPG?c=2"
        
        guard let url = URL(string: urlString) else { return }
        guard let data = try? Data(contentsOf: url) else { return }
        let image = UIImage(data: data)
        
        measure {
            let detections = yolo.predict(image: (image?.pixelBuffer(width: YOLO.inputWidth, height: YOLO.inputHeight))!)
            
            XCTAssert(detections.count == 1, "YOLO was not able to detect anything.")
            XCTAssert(detections[0].classIndex == 1, "YOLO was not able to detect a can.")
        }
    }
    
    func testIdentifierLoadingTime() throws {
        // This is an example of a performance test case.
        measure {
            let _ = Identifier()
        }
    }
    
    func testYOLOLoadingTime() throws {
        measure {
            let _ = YOLO()
        }
    }
}
