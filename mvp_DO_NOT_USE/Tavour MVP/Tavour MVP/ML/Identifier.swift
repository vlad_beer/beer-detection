//
//  Identifier.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 30.04.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import Foundation
import UIKit
import CoreML
import LASwift
import KDTree

struct Centroid {
    let index: Int!
    let vector: Vector
}

extension Array where Element == Double {
    init(with mlMultiArray: MLMultiArray) {
        self.init([Double](repeating: 0.0, count: mlMultiArray.count))
        for i in 0..<mlMultiArray.count {
            self[i] = mlMultiArray[i].doubleValue
        }
    }
}

extension Centroid: KDTreePoint {
    public static var dimensions: Int {
        return 512
    }
    
    public func kdDimension(_ dimension: Int) -> Double {
        return self.vector[dimension]
    }
    
    public func squaredDistance(to otherPoint: Centroid) -> Double {
        return sum(power((self.vector - otherPoint.vector), 2))
    }
}

class Identifier {
    
    let model: identifier_v4_fliplr_l2!
    let head: head_v4_fliplr_l2!
    
    let topL2 = 32
    static let confidenceThreshold: Float = 0.992

    typealias L2IndexEntry = (Int, Double)
    
    struct Prediction {
        var category: Category
        let confidence: Float
    }
    
    var trainMLMultiArrays: [MLMultiArray] = []

    public static let inputWidth = 256
    public static let inputHeight = 256
    public static let featureDum = 512
    
    private let _DEBUG_EXPANSION: Int = 1
    private var tree: KDTree<Centroid>!
    
    var categories: [Category] = []
    var id2category: [Int : Category] = [:]
    private var index: Matrix!
    private var index_tsvd: Matrix? = nil
    private var tsvd_matrix: Matrix? = nil
    
//    private func readClasses(filename: String) {
//        let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//        let fileURL = dir.appendingPathComponent("classes.txt")
////        try? FileManager.default.removeItem(at: fileURL)
//
//        var contents = try? String(contentsOf: fileURL, encoding: .utf8)
//        if contents == nil {
//            contents = try! String(contentsOf: URL(string: "http://35.224.13.56:5560/classes")!)
//            try! contents!.write(to: fileURL, atomically: false, encoding: .utf8)
//        }
//
//        contents!.enumerateLines { (line, _) -> () in
//            let components = line.components(separatedBy: " - ")
//            self.categories.append(Category(beerName: components[1], breweryName: components[0]))
//        }
//    }
//
//    private func readMat(filename: String) {
//        let tmpDir = NSURL(fileURLWithPath: NSTemporaryDirectory()) as! NSURL
//        let fileURL = tmpDir.appendingPathComponent("vectors")!
//
////        try? FileManager.default.removeItem(at: fileURL)
//
//        var vectors: [[Double]] = []
//
//        let data = try? Data(contentsOf: fileURL)
//        if let data = data {
//            vectors = NSKeyedUnarchiver.unarchiveObject(with: data) as! [[Double]]
//        } else {
//            let contents = try! String(contentsOf: URL(string: "http://35.224.13.56:5560/vectors")!)
//
//            var lines: [String] = []
//            contents.enumerateLines { (line, _) -> () in
//                lines.append(line)
//            }
//
//            var i = 0
//            for line in lines {
//                var vec: [Double] = []
//                for seq in line.components(separatedBy: " ") {
//                    vec.append(Double(seq)!)
//                }
//                vectors.append(vec)
//                i += 1
//            }
//
//            let encodedData = NSKeyedArchiver.archivedData(withRootObject: vectors)
//            try! encodedData.write(to: fileURL)
//        }
//
//        let matrix = Matrix(vectors)
//        index = matrix
//    }

    
    func readIndex() {
        let tmpDir = NSURL(fileURLWithPath: NSTemporaryDirectory())
        let fileURL = tmpDir.appendingPathComponent("index")!
//        try? FileManager.default.removeItem(at: fileURL)
        
        var vectors: [[Double]] = []
        let data = try? Data(contentsOf: fileURL)
        
        typealias JSON = [String : Any]
        var items: [JSON] =  []
        if let data = data {
            items = NSKeyedUnarchiver.unarchiveObject(with: data) as! [JSON]
        } else {
            var nextToken = 0
            while true {
                print("processing")
                print(nextToken)
                let contents = try! String(contentsOf: URL(string: "https://bg-indexes.stage-tavour.com?from=\(nextToken)&size=250")!)
                let data = contents.data(using: .utf8)!
                if let jsonArray = try? JSONSerialization.jsonObject(with: data) {
                    if let json = jsonArray as? JSON {
                        items.append(contentsOf: json["items"] as! [JSON])
                        if let nt = json["next_token"] as? Int {
                            nextToken = nt
                        } else {
                            break
                        }
                    }
                }
            }
            
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: items)
            try! encodedData.write(to: fileURL)
        }
        
        print("Total items count: \(items.count)")
        for i in 0..<items.count {
            let item = items[i]
            let category_id = item["category_id"] as! Int
            var beerName = item["beer_name"] as? String
            let breweryName = item["brewery_name"] as? String
            if beerName == nil || breweryName == nil {
                continue
            }
            let anchorURL = item["anchor_photo_url"] as? String
            let category = Category(id: category_id, beerName: beerName!, breweryName: breweryName!, anchorURL: anchorURL)
            categories.append(category)
            vectors.append((item["centroids"] as! [Any])[0] as! [Double])
            id2category[category_id] = category
//            if beerName.lowercased().contains("trick") {
//                print(beerName + breweryName)
//                print((item["centroids"] as! [Any])[0] as! [Double])
//            }
        }
        
        let matrix = Matrix(vectors)
        index = matrix
    }
    
    private func loadMainIndex() {
        let start = DispatchTime.now()
//        self.readClasses(filename: "classes_4.flip_lr.l2")
//        self.readMat(filename: "vectors_4.flip_lr.l2")
        readIndex()
//        index_tsvd = readMat(filename: "index_tsvd_v4")
//        tsvd_matrix = readMat(filename: "tsvd_matrix_v4")

        let end = DispatchTime.now()
        let timeDiff = end.uptimeNanoseconds - start.uptimeNanoseconds
        let timeInterval = Double(timeDiff) / 1_000_000_000
        print("[INFO] Index loaded in \(timeInterval) seconds.")
    }
    
    private func buildKDTree() {
        let start = DispatchTime.now()
        var centroids: [Centroid] = []
        for i in 0..<categories.count {
            centroids.append(Centroid(index: i, vector: index![row:i]))
        }
        
        tree = KDTree(values: centroids)
        let end = DispatchTime.now()
        let timeDiff = end.uptimeNanoseconds - start.uptimeNanoseconds
        let timeInterval = Double(timeDiff) / 1_000_000_000
        print("[TIME] Tree built in \(timeInterval) seconds.")
    }
    
    private func loadData() {
        self.loadMainIndex()
        print("Loaded main index.")
        self.buildKDTree()
        print("Built KDTree")
        
        for i in 0..<self.categories.count {
            self.trainMLMultiArrays.append(self.vec2ml(vec: self.index![row:i]))
        }
    
    }

    public init() {
        let start = DispatchTime.now()
        model = identifier_v4_fliplr_l2()
        head = head_v4_fliplr_l2()
        let end = DispatchTime.now()
        let timeDiff = end.uptimeNanoseconds - start.uptimeNanoseconds
        let timeInterval = Double(timeDiff) / 1_000_000_000
        print("[TIME] Identification models loaded in \(timeInterval) seconds.")
    
        loadData()
    }
    
    private func vec2ml(vec: Vector) -> MLMultiArray {
        let mlArray = try! MLMultiArray(shape: [NSNumber(value: vec.count)], dataType: MLMultiArrayDataType.float32)
        for i in 0..<Identifier.featureDum {
             mlArray[i] = NSNumber(value: vec[i])
        }
        return mlArray
    }

    private func calcEmbedding(image: CVPixelBuffer) -> MLMultiArray? {
        let start = DispatchTime.now()
        let output = try? model.prediction(image: image)
        print("[TIME] Branch inference took \(Double(DispatchTime.now().uptimeNanoseconds - start.uptimeNanoseconds) / 1_000_000_000) seconds.")
        return output?.output
    }
    
    private func findBestCandidates(amongst l2Candidates: [L2IndexEntry], topK: Int) -> [L2IndexEntry] {
        // Finding topK closest items
        var candidates: [L2IndexEntry] = []
        for i in 0..<topK {
            candidates.append(l2Candidates[i])
        }

        func findWorst() -> Int {
            var worstIndex = -1
            var worstL2: Double = 0
            for i in 0..<topK {
                if candidates[i].1 > worstL2 {
                    worstL2 = candidates[i].1
                    worstIndex = i
                }
            }
            return worstIndex
        }
        
        var worstIndex = findWorst()
        var worstValue = candidates[worstIndex].1
        for i in topK..<l2Candidates.count {
            if l2Candidates[i].1 < worstValue {
                candidates[worstIndex] = l2Candidates[i]
                worstIndex = findWorst()
                worstValue = candidates[worstIndex].1
            }
        }
        
        return candidates
    }
    
    public func predict(image: CVPixelBuffer, topK: Int) -> [Prediction]? {
        guard let features = calcEmbedding(image: image) else { return nil }
    
        let start = DispatchTime.now()

        let embedding = Array(with: features)

//        var candidates: [L2IndexEntry] = []
//        if let tsvd_matrix = tsvd_matrix, let index_tsvd = index_tsvd { // Emabling TruncatedSVD
//            let embeddingMatrix = Matrix([embedding])
//            let embeddingTSVD = (embeddingMatrix * tsvd_matrix)[row: 0]
//
//            // Get L2 distances
//            let l2 = sum(power(index_tsvd - embeddingTSVD, 2), .Row)
//            var pairs: [L2IndexEntry] = Array(repeating: (0, 0.0), count: l2.count)
//            for i in 0..<l2.count { // Zipping
//                pairs[i] = (i, l2[i])
//            }
//
//            // Getting topL2 elements
//            var heap = Heap(array: pairs) { (pairA: L2IndexEntry, pairB: L2IndexEntry) -> Bool in
//                return pairA.1 < pairB.1
//            }
//
//            for _ in 0..<topL2 {
//                let entry = heap.remove()!
//                let cl2 = sum(power((embedding - self.index![row: entry.0]), 2))
//                candidates.append((entry.0, cl2))
//            }
//        } else {
//            let l2 = sum(power(self.index! - Vector(embedding), 2), .Row)
//            candidates = Array(repeating: (0, 0.0), count: l2.count)
//            for i in 0..<l2.count { // Zipping
//                candidates[i] = (i, l2[i])
//            }
//        }
//
//        // Finding topK candidates
//        let bestCandidates = findBestCandidates(amongst: candidates, topK: topK)
        
        let bestCandidates = tree.nearestK(topK, to: Centroid(index: nil, vector: embedding))

        var predictions: [Prediction] = []
        for candidate in bestCandidates {
            guard let confidence = try? head.prediction(
                features1: trainMLMultiArrays[candidate.index!],
                features2: features).probability[0].floatValue else { return nil }
            let category = categories[candidate.index!]
            predictions.append(Prediction(category: category, confidence: confidence))
        }
        predictions.sort(by: {
            return $0.confidence > $1.confidence
        })
        
        print("[TIME] Embedding identification took \(Double(DispatchTime.now().uptimeNanoseconds - start.uptimeNanoseconds) / 1_000_000_000) seconds.")
        return predictions
    }
}
