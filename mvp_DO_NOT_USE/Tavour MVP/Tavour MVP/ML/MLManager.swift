//
//  MLManager.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 30.04.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit
import VideoToolbox

class MLManager {
    private var yolo: YOLO?
    private var identifier: Identifier?
    
    private let supportedObjectTypes = [0, 1, 3] // Bottle, can, label
    
    let topK = 10 // Number of candidates to consider in identification
    let labelPropagationThreshold: Float = 0.7
    
    let group = DispatchGroup()

    static let shared = MLManager()

    func category(id: Int) -> Category {
        return identifier!.id2category[id]!
    }
    
    public func initialize() {
        self.group.enter()
        self.group.enter()
        
        DispatchQueue.global().async {
            self.yolo = YOLO()
            print("[INFO] YOLO done.")
            self.group.leave()
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.identifier = Identifier()
            print("[INFO] Identifier & index done.")
            self.group.leave()
        }
    }
    
    public var supportedClasses: [Category] {
        return identifier!.categories
    }

    private init() {
    }
    
    struct Prediction {
        var location: CGRect
        var candidates: [Identifier.Prediction]
        let classIndex: Int
        let classScore: Float
        var crop: UIImage
        var labelPropagated: Bool
        var tavourRating: Float?
        var userRating: Int?
    }
    
    func predict(image: UIImage) -> [Prediction] {
        let imagePortrait = image.fixedOrientation()!
        
        group.wait()
        
        guard let yolo = yolo else {
            return []
        }
        
        guard let pixelBuffer = imagePortrait.pixelBuffer(width: YOLO.inputWidth, height: YOLO.inputHeight) else {
            return []
        }

        let boundingBoxes = yolo.predict(image: pixelBuffer)

        let start = DispatchTime.now()
        var predictions: [Prediction] = []
        var totalBoundingBoxes = 0
        var bottles: [YOLO.Prediction] = []
        
        let queue = OperationQueue()
        queue.name = "com.company.concurrent"
        queue.qualityOfService = .userInitiated
        queue.maxConcurrentOperationCount = 6

        let cg = imagePortrait.cgImage!
        let color = UIColor(red: 118.42105242/255.0, green: 101.18178984/255.0, blue: 83.48132339/255.0, alpha: 1.0).cgColor
        for boundingBox in boundingBoxes {

            let scaledBoundingBox = scale(bbox: boundingBox.rect, toWidth: image.size.width, toHeight: image.size.height)
            
            // Not supported
            if !supportedObjectTypes.contains(boundingBox.classIndex) {
                continue
            }
            
            // Bottle
            if boundingBox.classIndex == 0 {
                bottles.append(YOLO.Prediction(classIndex: 0, score: boundingBox.score, rect: scaledBoundingBox))
                continue
            }
            
            queue.addOperation {
                let start = DispatchTime.now()
                            
                // Creating a pixel buffer
                guard let cgCrop = cg.cropping(to: scaledBoundingBox) else { return }
                let context = CGContext(data: nil,
                                       width: 256,
                                       height: 256,
                                       bitsPerComponent: cg.bitsPerComponent,
                                       bytesPerRow: cg.bytesPerRow,
                                       space: cg.colorSpace ?? CGColorSpace(name: CGColorSpace.sRGB)!,
                                       bitmapInfo: cg.bitmapInfo.rawValue)
                context?.interpolationQuality = .medium
                let aspectRatio = scaledBoundingBox.height / scaledBoundingBox.width
                let width: CGFloat = 256 / aspectRatio
                context?.setFillColor(color)
                context?.fill(CGRect(x: 0, y: 0, width: 256, height: 256))
                context?.draw(cgCrop, in: CGRect(origin: CGPoint(x: (256 - width) / 2.0, y: 0), size: CGSize(width: width, height: 256)))
                
                guard let scaledImage = context?.makeImage() else { return }
                let pixelBuffer = scaledImage.pixelBuffer(width: Identifier.inputWidth, height: Identifier.inputHeight, orientation: .up)!
    //            savePixelBuffer(pixelBuffer: pixelBuffer)
            
                print("[TIME] Crop resizing took  \(Double(DispatchTime.now().uptimeNanoseconds - start.uptimeNanoseconds) / 1_000_000_000) seconds.")
                
                totalBoundingBoxes += 1
                guard let identificationResults = self.identifier!.predict(image: pixelBuffer, topK: self.topK) else { return }
                
                predictions.append(Prediction(location: scaledBoundingBox, candidates: identificationResults, classIndex: boundingBox.classIndex, classScore: boundingBox.score, crop: UIImage(cgImage: cgCrop), labelPropagated: false))
            }
        }
        
        queue.waitUntilAllOperationsAreFinished()
        
        propagateLabels(predictions: &predictions)
        
        // Matching bottles
        outer_cycle: for i in 0..<predictions.count {
            let prediction = predictions[i]
            if prediction.classIndex == 3 { // "label"
                let rect = prediction.location
                for bottle in bottles {
                    if rect.midX > bottle.rect.minX && rect.midX < bottle.rect.maxX &&
                        rect.midY > bottle.rect.minY && rect.midY < bottle.rect.maxY {
                        predictions[i] = Prediction(location: bottle.rect, candidates: prediction.candidates, classIndex: 0, classScore: prediction.classScore, crop: prediction.crop, labelPropagated: false)
                        continue outer_cycle
                    }
                }
            }
        }
        
        let end = DispatchTime.now()
        let timeDiff = end.uptimeNanoseconds - start.uptimeNanoseconds
        let timeInterval = Double(timeDiff) / 1_000_000_000
        print("[TIME] Identification took \(timeInterval) seconds (\(timeInterval / Double(totalBoundingBoxes)) per image).")
        
        return predictions
    }
    
    func propagateLabels(predictions: inout [Prediction]) {
        let start = DispatchTime.now()
        
        predictions.sort(by: {
            return $0.candidates[0].confidence > $1.candidates[0].confidence
        })
        
        func areAdjacent(A: CGRect, B: CGRect) -> Bool {
            let errorMultiplier: CGFloat = 1.25
            if B.midX > A.minX && B.midX < A.maxX && abs(A.midY - B.midY) <= (A.height + B.height) * 0.5 * errorMultiplier {
                return true
            } else if B.midY > A.minY && B.midY < A.maxY && abs(A.midX - B.midX) <= (A.width + B.width) * 0.5 * errorMultiplier {
                return true
            } else {
                return false
            }
        }
        
        let N = predictions.count
        var adjacencyList: [[Int]] = Array(repeating: [], count: N)
        for i in 0..<N {
            for j in i+1..<N {
                if  areAdjacent(A: predictions[i].location, B: predictions[j].location) {
                    adjacencyList[i].append(j)
                    adjacencyList[j].append(i)
                }
            }
        }

        var used = Array(repeating: false, count: N)

        func dfs(index: Int, brewery: String? = nil) {
            let prediction = predictions[index]
            if let brewery = brewery {
                
                for i in 0..<prediction.candidates.count {
                    let candidate = prediction.candidates[i]
                    if candidate.confidence < labelPropagationThreshold {
                        break
                    }
                    if candidate.category.breweryName == brewery {
                        used[index] = true
                        predictions[index].candidates.remove(at: i)
                        predictions[index].candidates.insert(Identifier.Prediction(category: candidate.category, confidence: 1.0), at: 0)
                        predictions[index].labelPropagated = true
                        for j in adjacencyList[index] {
                            if !used[j] {
                                dfs(index: j, brewery: candidate.category.breweryName)
                            }
                        }
                        break
                    }
                }
                
            } else if prediction.candidates[0].confidence > Identifier.confidenceThreshold {
                used[index] = true
                for j in adjacencyList[index] {
                    if !used[j] {
                        dfs(index: j, brewery: prediction.candidates[0].category.breweryName)
                    }
                }
            }
        }
        
        for i in 0..<N {
            if !used[i] {
                dfs(index: i)
            }
        }
        
        let end = DispatchTime.now()
        let timeDiff = end.uptimeNanoseconds - start.uptimeNanoseconds
        let timeInterval = Double(timeDiff) / 1_000_000_000
        print("[TIME] Label propagation algorithm finished in \(timeInterval) seconds.")
    }
}

extension UIImage {
    public func padTo(width: CGFloat, height: CGFloat) -> UIImage {
        let resultingWidth = max(size.width, size.height * width / height)
        let resultingHeight = max(size.height, size.width * height / width)
        
        let horizontalOffset = (resultingWidth - size.width) / 2.0
        let verticalOffset = (resultingHeight - size.height) / 2.0
        
        let view: UIView = UIView(frame: CGRect(x: 0, y: 0, width: resultingWidth, height: resultingHeight))
        let imageView: UIImageView = UIImageView(frame: CGRect(x: horizontalOffset, y: verticalOffset, width: size.width, height: size.height))
        imageView.image = self
        view.addSubview(imageView)
        view.backgroundColor = UIColor.init(red: 118.42105242/255.0, green: 101.18178984/255.0, blue: 83.48132339/255.0, alpha: 1.0)
        return image(with: view)!
    }
}

func scale(bbox: CGRect, toWidth width: CGFloat, toHeight height: CGFloat) -> CGRect {
    let scaleX = width / CGFloat(YOLO.inputWidth)
    let scaleY = height / CGFloat(YOLO.inputHeight)
    
    var rect = bbox
    
    rect.origin.x *= scaleX
    rect.origin.y *= scaleY
    rect.size.width *= scaleX
    rect.size.height *= scaleY
    
    return rect
}

func pad(boundingBox: CGRect, delta: CGFloat) -> CGRect {
    var rect = boundingBox
    rect.origin.x -= rect.width * delta
    rect.origin.y -= rect.height * delta
    rect.size.width *= 1 + 2 * delta
    rect.size.height *= 1 + 2 * delta
    
    return rect
}

func image(with view: UIView) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
    defer { UIGraphicsEndImageContext() }
    if let context = UIGraphicsGetCurrentContext() {
        view.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        return image
    }
    return nil
}
