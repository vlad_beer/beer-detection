//
//  FAdd.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 25.03.2020.
//  Copyright © 2020. All rights reserved.
//

import Foundation
import Accelerate
import CoreML

@objc (FAdd) class FAdd: NSObject, MLCustomLayer {

    required init(parameters: [String : Any]) throws {
        super.init()
    }
    
    func setWeightData(_ weights: [Data]) throws {
    }
    
    func outputShapes(forInputShapes inputShapes: [[NSNumber]]) throws
        -> [[NSNumber]] {
        let outputShapes = [inputShapes[0]]
        return outputShapes
    }

    func evaluate(inputs: [MLMultiArray], outputs: [MLMultiArray]) throws {
        let iptr1 = UnsafeMutablePointer<Float>(OpaquePointer(inputs[0].dataPointer))
        let iptr2 = UnsafeMutablePointer<Float>(OpaquePointer(inputs[1].dataPointer))
        let optr = UnsafeMutablePointer<Float>(OpaquePointer(outputs[0].dataPointer))

        let stride = vDSP_Stride(1)
        let length = vDSP_Length(512)
    
        vDSP_vadd(iptr1, stride, iptr2, stride, optr, stride, length)
    }
}
