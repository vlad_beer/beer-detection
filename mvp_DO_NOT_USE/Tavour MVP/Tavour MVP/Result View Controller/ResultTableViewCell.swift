//
//  ResultTableViewCell.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 01.05.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit
import Cosmos

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as? UIViewController
            }
        }
        return nil
    }
}

extension UIView {
    func parentView<T: UIView>(of type: T.Type) -> T? {
        guard let view = superview else {
            return nil
        }
        return (view as? T) ?? view.parentView(of: T.self)
    }
}

extension UITableViewCell {
    var tableView: UITableView? {
        return parentView(of: UITableView.self)
    }
}

class ResultTableViewCell: UITableViewCell {
    @IBOutlet weak var coreView: UIView!
    @IBOutlet weak var photoImageView: UIImageView!
    
    @IBOutlet weak var beerTitleLabel: UILabel!
    @IBOutlet weak var breweryNameLabel: UILabel!
    @IBOutlet weak var beerInfoLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var ratingView: CosmosView!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var dislikeButton: UIButton!
    
    @IBOutlet weak var tavourRatingStackView: UIStackView!
    var probability: Float = 0.0
    var identified: Bool = false
    var index: Int = -1
    
    var hapticFeedbackDelegate: HapticFeedbackProtocol!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        ratingView.settings.passTouchesToSuperview = false
        ratingView.settings.disablePanGestures = true
        ratingView.didTouchCosmos = { rating in
            self.hapticFeedbackDelegate.registerInteraction()
        }
        
        ratingView.didFinishTouchingCosmos = { rating in
            self.hapticFeedbackDelegate.playChimes()
            self.hapticFeedbackDelegate.didRate(index: self.index, rating: Float(rating))
        }
        ratingView.settings.fillMode = .precise
        ratingView.rating = 0
        ratingView.settings.minTouchRating = 0
    }

    override public func prepareForReuse() {
      // Ensures the reused cosmos view is as good as new
      ratingView.prepareForReuse()
    }
    
    @IBAction func report(_ sender: Any) {
        (parentViewController! as! ResultViewController).reportError(index: (sender as! UIButton).tag)
    }
    
    @IBAction func like(_ sender: Any) {
        self.hapticFeedbackDelegate.didLike(like: true, index: index)
        self.likeButton.isHidden = true
        self.dislikeButton.isHidden = true
        self.tavourRatingStackView.isHidden = false
        self.ratingView.isHidden = false
        self.reportButton.isHidden = false
    }
    
    @IBAction func dislike(_ sender: Any) {
        self.hapticFeedbackDelegate.didLike(like: false, index: index)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
