//
//  ResultViewController.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 30.04.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit
import AVFoundation

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.jpegData(compressionQuality: 0.3) else { return nil }
        return "data:image/jpeg;base64," + imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
}

class ResultViewController: UIViewController, ResultOverrideProtocol {
    var isCellSelectionEnabled: Bool = true
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var pullView: UIView!
    @IBOutlet weak var extrasBar: UIView!
    
    var predictionResults: [MLManager.Prediction]!
    var image: UIImage!
    let player = AVQueuePlayer()
    var ratings: [Float] = []
    
    // Tags
    var tagViews: [UIView] = []
    var selectedTagIndex: Int?
    let tagSize: CGFloat = 16
    
    var selectedBeer: MLManager.Prediction!
    var selectedIndex: Int = 0
    
    let deltaH: CGFloat = 0.05
    var offsetH: CGFloat = -1
    
    var isLoaded: Bool = false

    func playChimes() {
        if let url = Bundle.main.url(forResource: "chimes", withExtension: "m4a") {
            player.removeAllItems()
            player.insert(AVPlayerItem(url: url), after: nil)
            player.play()
        }
    }
    
    private func configureViews() {
        let distanceToBounds: CGFloat = 24
        let distanceToPuller: CGFloat = 12
        
        let fullWidth = self.view.frame.width - 2 * distanceToBounds
        
        image = { // Cropping
            let minY = predictionResults.reduce(1e5) { (result, prediction) -> CGFloat in
                return min(result, prediction.location.minY)
            }
            let maxY = predictionResults.reduce(0) { (result, prediction) -> CGFloat in
                return max(result, prediction.location.maxY)
            }
            offsetH = max(0, minY - image.size.height * deltaH)
            let cgImage = image.fixedOrientation()!.cgImage
            let croppedCGImage = cgImage?.cropping(to: CGRect(x: 0, y: offsetH, width: image.size.width, height:
                min(image.size.height, maxY + image.size.height * deltaH) - offsetH))
            let croppedImage = UIImage(cgImage: croppedCGImage!)
            
            return croppedImage
        }()
        
        mainImageView.image = image
        mainImageView.sizeToFit()
        
        let anisotropy = mainImageView.frame.height / mainImageView.frame.width
        
        if anisotropy > 1 {
            let imageViewWidth = fullWidth / anisotropy
            mainImageView.frame = CGRect(
                x: distanceToBounds + (fullWidth / 2.0 - imageViewWidth / 2.0),
                y: pullView.frame.maxY + distanceToPuller,
                width: imageViewWidth,
                height: fullWidth)
        } else {
            let imageViewHeight = fullWidth * anisotropy
            mainImageView.frame = CGRect(
                x: distanceToBounds,
                y: pullView.frame.maxY + distanceToPuller,
                width: fullWidth,
                height: imageViewHeight)
        }
        
//        tableViewWrapper.frame = CGRect(x: 0, y: mainImageView.frame.maxY + distanceToPuller, width: self.view.frame.width, height: extrasBar.frame.minY - (mainImageView.frame.maxY + distanceToPuller))
//        tableViewWrapper.clipsToBounds = true
        
        tableView.frame = CGRect(x: 0, y: mainImageView.frame.maxY + distanceToPuller, width: self.view.frame.width, height: extrasBar.frame.minY - (mainImageView.frame.maxY + distanceToPuller))
        tableView.setNeedsDisplay()
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
        self.view.updateConstraints()
        tableView.updateConstraints()
//        print(tableView.frame.maxY, extrasBar.frame.maxY)
//        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 98, bottom: 0, right: 0)
        // tableView.contentInset = ...

        configureViews()
    }
    
    func setup(predictionResults: [MLManager.Prediction]) {
        self.predictionResults = predictionResults
        ratings = Array(repeating: 0, count: predictionResults.count)
        
        enumerateItems()
        
        self.predictionResults.shuffle()
    }
    
    func enumerateItems() {
        let start = DispatchTime.now()
        
        predictionResults.sort(by: {
            return $0.location.minX < $1.location.minX
        })
        
        var chains: [(CGFloat, [Int])] = []
        
        var used: [Bool] = Array(repeating: false, count: predictionResults.count)
        
        for index in 0..<used.count {
            if !used[index] {
                var chain = [index]
                var pivotIndex = index
                outer_cycle: while true {
                    let location = predictionResults[pivotIndex].location
                    let upperBound = location.maxY + location.height * 0.25
                    let lowerBound = location.maxY - location.height * 0.25
                    
                    for i in pivotIndex + 1..<used.count {
                        if used[i] {
                            continue
                        }
                        let maxY = predictionResults[i].location.maxY
                        if maxY >= lowerBound && maxY <= upperBound {
                            used[i] = true
                            chain.append(i)
                            pivotIndex = i
                            continue outer_cycle
                        }
                    }
                    break
                }
                
                // Lining up
                if chain.count > 1 {
                    var xs: [CGFloat] = []
                    var ys: [CGFloat] = []
                    var xys: [CGFloat] = []
                    var x2s: [CGFloat] = []
                    for i in chain {
                        let x = predictionResults[i].location.midX
                        let y = predictionResults[i].location.maxY
                        xs.append(x)
                        ys.append(y)
                        xys.append(x * y)
                        x2s.append(x * x)
                    }
                    
                    func mean(_ array: [CGFloat]) -> CGFloat {
                        return array.reduce(0, +) / CGFloat(array.count)
                    }
                    
                    let mean_xs = mean(xs)
                    let mean_ys = mean(ys)
                    let m = (mean(xys) - mean_xs * mean_ys) / (mean(x2s) - mean_xs * mean_xs)
                    let b = mean_ys - m * mean_xs
                    
                    for i in chain {
                        let x = predictionResults[i].location.midX
                        let y = m * x + b
                        var rect = predictionResults[i].location
                        rect.size.height = max(0, rect.size.height - rect.maxY + y)
                        predictionResults[i] = MLManager.Prediction(
                            location: rect,
                            candidates: predictionResults[i].candidates,
                            classIndex: predictionResults[i].classIndex,
                            classScore: predictionResults[i].classScore,
                            crop: predictionResults[i].crop,
                            labelPropagated: false)
                    }
                }
                
                chains.append((predictionResults[index].location.maxY, chain))
            }
        }
        
        chains.sort(by: {
            $0.0 < $1.0
        })
        
        var newPredictions: [MLManager.Prediction] = []
        for chain in chains {
            for index in chain.1 {
                newPredictions.append(predictionResults[index])
            }
        }
        
        predictionResults = newPredictions
        
        let end = DispatchTime.now()
        let timeDiff = end.uptimeNanoseconds - start.uptimeNanoseconds
        let timeInterval = Double(timeDiff) / 1_000_000_000
        print("[TIME] Enumeration finished in \(timeInterval) seconds.")
    }

    @IBAction func reportERT(_ sender: Any) {
        let alert = UIAlertController(title: "[Tavour-Only] ERT Session", message: "Images submitted during ERT sessions will be manually annotated and should only be submitted for benchmarking purposes. Before submitting, make sure that the image is of good quality, centered and isn't blurred.", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Proceed", style: .destructive, handler: { (action) in
            alert.dismiss(animated: true) {
                if let image = self.image {
//                    let json = self.toJSON()
                    self.POSTRequest(jsonObject: self.toJSON(),
                        url: "https://bg-data.stage-tavour.com/rest/ert",
//                                     message: json,
                                     errorHandler: { (message) in
                                        self.presentAlert(title: "Error", message: "Something went wrong. Please try again.")
                                    }, completionHandler: { () in
                                        self.presentAlert(title: "Success",
                                            message: "ERT session successfully recorded.")
                                    })
                    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true)
        }))
        
        show(alert, sender: self)
    }
    func toJSON() -> JSON_t {
        var json: JSON_t = [:]

        // Results
        var results: [[String: Any]] = []
        for i in 0..<predictionResults.count {
            let prediction = predictionResults[i]
            let rect = prediction.location
            var candidates: [[String: Any]] = []
            for candidate in prediction.candidates {
                candidates.append(["id": candidate.category.id, "class_confidence": candidate.confidence])
            }
            results.append([
                "index": i,
                "bounding_box": [rect.minX, rect.minY, rect.maxX, rect.maxY],
                "type": labels[prediction.classIndex],
                "type_confidence": prediction.classScore,
                "candidates": candidates,
                "label_propagated": prediction.labelPropagated
            ])
        }
        json["results"] = results
        json["image_encoding"] = image.toBase64()!
        json["notes"] = "Alamofire"
        
//        let data = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
//        let convertedString = String(data: data!, encoding: .utf8)
        
        return ["json_manifest": json]
    }
    
    @objc func didTapTag(sender: UITapGestureRecognizer) {
        let location = sender.location(in: mainImageView)

        // Adding tags
        for i in 0..<predictionResults.count {
            let prediction = predictionResults[i]

            var rect = prediction.location
            rect.size.height += tagSize / 2.0
            
            if rect.contains(location) {
                tableView.setContentOffset(CGPoint(x: 0, y: max(0, 140 * i - 70)), animated: true)
                break
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isLoaded {
            return
        }
        
        print("ResultViewController.swift: viewWillAppear")
        super.viewDidAppear(animated)

        let scale = image.size.width / mainImageView.frame.width

        // Adding tags
        for i in 0..<predictionResults.count {
            let prediction = predictionResults[i]

            var rect = prediction.location
            rect.origin.x /= scale
            rect.origin.y = (rect.origin.y - offsetH) / scale
            rect.size.width /= scale
            rect.size.height /= scale
            predictionResults[i].location = rect

            let center = CGPoint(x: rect.midX, y: rect.maxY)

            let label = UILabel(frame: CGRect(x: 0, y: 2, width: tagSize, height: tagSize))
            label.text = String(i + 1)
            label.font = UIFont(name: "OpenSans-Bold", size: 8)
            label.textAlignment = .center
            label.textColor = .white
            
            let imageView = UIImageView(image: UIImage(named: "tag_gray")!)
            imageView.frame = label.frame
            
            let wrapper = UIView(frame: CGRect(x: center.x - tagSize / 2.0, y: center.y - tagSize / 2.0, width: tagSize, height: tagSize))
            wrapper.addSubview(imageView)
            wrapper.addSubview(label)
            
            mainImageView.addSubview(wrapper)
            tagViews.append(wrapper)
        }
        
        let gr = UITapGestureRecognizer(target: self, action: #selector(didTapTag(sender:)))
        mainImageView.isUserInteractionEnabled = true
        mainImageView.addGestureRecognizer(gr)
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: corePhotoView.frame.width, height: corePhotoView.frame.height))
//        view.clipsToBounds = true
//        view.addSubview(imageView)
//
//        corePhotoView.addSubview(view)
////        corePhotoView.layer.shadowColor = UIColor.black.cgColor
////        corePhotoView.layer.shadowRadius = 10
////        corePhotoView.layer.shadowOpacity = 0.3
////        corePhotoView.layer.shadowOffset = CGSize(width: 0, height: 0)
////        corePhotoView.layer.masksToBounds = false
//        corePhotoView.layer.cornerRadius = 12
//        corePhotoView.clipsToBounds = true
//
//        reportButton.superview?.bringSubviewToFront(reportButton)
//
        DispatchQueue.main.async {
            if let presentingVC = self.presentingViewController as? CameraViewController {
                presentingVC.manageUIElements(isHidden: false)
                presentingVC.manageSession(start: true)
                presentingVC.manageSpinner(isHidden: true)
                presentingVC.resultImageView.isHidden = true
            }
        }
        isLoaded = true
        selectTag(index: 0)
        
        self.POSTRequest(jsonObject: ["encoded_image": self.image.toBase64()!], url: "https://bg-image-intake.stage-tavour.com/submit_image", verbose: false, errorHandler: { (result) in
//            self.presentAlert(title: "Error sending full image", message: nil)
        }) {
//            self.presentAlert(title: "Success sending full image", message: nil)
        }
    }

    
    func reportError(index: Int) {
        let prediction = predictionResults[index]
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Wrong Beer", style: .destructive, handler: { (action) in
            alert.dismiss(animated: true) {
                self.selectedIndex = index
                self.selectedBeer = prediction
                self.performSegue(withIdentifier: "error_handling_segue", sender: self)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Share", style: .default, handler: { (action) in
            alert.dismiss(animated: true) {
                self.selectedIndex = index
                self.selectedBeer = prediction
                self.performSegue(withIdentifier: "error_handling_segue", sender: self)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true) {
                
            }
        }))
        
        self.present(alert, animated: true)
    }
    
    func confirm(image: UIImage, beer: String) {
        let alert = UIAlertController(title: "Confirm", message: "Do you confirm this is \n\(beer)?", preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: "Confirm", style: .default, handler: { (action) in
            alert.dismiss(animated: true) {
//                self.report(image: image, url: "http://35.224.13.56:5560/report_unknown_confirmed", message: beer, compress: false)
            }
        })
        alert.addAction(action)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true)
        }))
        
        self.present(alert, animated: true)
    }
    
    func overrideResult(beerName: String, breweryName: String) {
        let index = selectedIndex
        predictionResults[index].candidates[0] = Identifier.Prediction(category: Category(id: -1, beerName: beerName, breweryName: breweryName, anchorURL: nil), confidence: 1.0)
        let prediction = predictionResults[index]
        
        let rowIndexText = String(index + 1)
        
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ResultTableViewCell
        cell?.beerTitleLabel.text = " " + rowIndexText + ". " + prediction.candidates[0].category.beerName
        cell?.breweryNameLabel.text = prediction.candidates[0].category.breweryName
        cell?.photoImageView.image = prediction.crop
        cell?.ratingView.isHidden = false
        cell?.reportButton.isHidden = false
        cell?.tavourRatingStackView.isHidden = false
        cell?.likeButton.isHidden = true
        cell?.dislikeButton.isHidden = true
    }
    
    func submitToServer(errorType: String, user_id: Int = 0, overrideID: Int? = nil, breweryName: String, beerName: String, verbose: Bool = true) {
        var json: [String: Any] = [:]
        let prediction = predictionResults[selectedIndex]
        
        // Results
        var results: [[String: Any]] = []
        for i in 0..<prediction.candidates.count {
            let candidate = prediction.candidates[i]
            results.append([
                "category_id": candidate.category.id,
                "confidence": candidate.confidence
            ])
        }
        json["top_categories"] = results
        json["image_encoding"] = prediction.crop.toBase64()!
        json["user_id"] = 0
        json["error_type"] = errorType
        
        var additionalData: [String: Any] = [:]
        if let overrideID = overrideID {
            additionalData["override_id"] = overrideID
        } else if errorType == "beer_created" {
            additionalData["user_specified_brewery"] = breweryName
            additionalData["user_specified_beer"] = beerName
        }
        json["additional_data"] = additionalData
        
        print(json)
//        let data = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
//        let convertedString = String(data: data!, encoding: .utf8)
//
        POSTRequest(jsonObject: json, url: "https://bg-data.stage-tavour.com/rest/imageClassificationErrors", verbose: verbose, errorHandler: { (nil) in
            self.presentAlert(title: "Error", message: "Something went wrong. Please try again.")
        }) {
            self.presentAlert(title: "Thanks!", message: "Thank you for helping make our service great!")
        }
    }
    
    func selectTag(index: Int) {
        if let selectedTagIndex = selectedTagIndex, selectedTagIndex == index {
            return
        }
        if let selectedTagIndex = selectedTagIndex {
            let view = tagViews[selectedTagIndex]
            for subview in view.subviews {
                let rated = ratings[selectedTagIndex] > 0
                if let subview = subview as? UILabel {
                    subview.textColor = rated ? .black : .white
                } else if let subview = subview as? UIImageView {
                    subview.image = UIImage(named: rated ? "tag_yellow" : "tag_gray")
                }
            }

            let cell = tableView.cellForRow(at: IndexPath(row: selectedTagIndex, section: 0)) as? ResultTableViewCell
            cell?.beerTitleLabel.backgroundColor = UIColor.clear
        }

        selectedTagIndex = index
        let view = tagViews[index]
        for subview in view.subviews {
            if let subview = subview as? UILabel {
                subview.textColor = .black
            } else if let subview = subview as? UIImageView {
                subview.image = UIImage(named: "tag_white")
            }
        }

        let cell = tableView.cellForRow(at: IndexPath(row: selectedTagIndex!, section: 0)) as? ResultTableViewCell
        cell?.beerTitleLabel.backgroundColor = .systemGray5
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("ResultViewController.swift: viewWillDisappear")
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("ResultViewController.swift: viewDidDisappear")
        super.viewDidDisappear(animated)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ResultViewController: UITableViewDelegate, UITableViewDataSource {

    func isIdentified(index: Int) -> Bool {
        return predictionResults[index].candidates[0].confidence > Identifier.confidenceThreshold
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "result_table_view_cell", for: indexPath) as! ResultTableViewCell
        
        let index = indexPath.row
        
        let prediction = predictionResults[index]
    
        let predictedClass = prediction.candidates[0].category
        
        let rowIndexText = String(index + 1)
        
        cell.beerTitleLabel.text = " \(rowIndexText). \(predictedClass.beerName) "
        cell.beerTitleLabel.layer.cornerRadius = 6
        cell.breweryNameLabel.text = "\(predictedClass.breweryName)"

        let identified = isIdentified(index: index)
        cell.identified = identified
        cell.ratingView.isHidden = !identified
        cell.reportButton.isHidden = !identified
        cell.tavourRatingStackView.isHidden = !identified
        cell.likeButton.isHidden = identified
        cell.dislikeButton.isHidden = identified
        
        cell.reportButton.tag = index
        cell.index = index
        cell.ratingView.rating = Double(ratings[index])
        
        cell.userInteractionEnabledWhileDragging = false
        let beerTypes = ["American Pale Ale", "American IPA", "Imperial Stout", "Brown Ale", "India Pale Ale", "American Wheat", "Farmhouse Ale"]
        let beerType = beerTypes[index % beerTypes.count]
        cell.beerInfoLabel.text = beerType
//        cell.scoreLabel.text = String(format: "%.2f", 100 * prediction.candidates[0].confidence)
        cell.probability = prediction.candidates[0].confidence
        cell.photoImageView.image = prediction.crop
        
        cell.hapticFeedbackDelegate = self
        
        return cell
    }
    
    func didRate(index: Int, rating: Float) {
        ratings[index] = rating
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("Did select")
//        if !isCellSelectionEnabled {
//            return
//        }
//        let index = indexPath.row
//        let predictionIndex = showIdentifiedOnly ? identifiedIndices[index] : index
//        let prediction = predictionResults[predictionIndex]
//        let image = UIImage(named: "sample_thumbnail_\(index % 19)")
//
//        let comps = prediction.className.components(separatedBy: " - ")
//
//        self.beerInfo = DetailViewController.SelectedBeerInfo(image: image!, beerTitle: comps[1], breweryName: comps[0])
//
//        performSegue(withIdentifier: "show_detail", sender: self)
        
//        self.selectedIndex = index
//        self.selectedBeer = prediction
//
////        self.performSegue(withIdentifier: "show_detail", sender: self)
//
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return predictionResults.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if predictionResults.count <= 1 {
            return
        }
        
        let rowHeight = self.tableView(scrollView as! UITableView, heightForRowAt: IndexPath(row: 0, section: 0))
        
        // Selecting bubble
        let scrollDelta = rowHeight * (CGFloat(predictionResults.count) - 1) / CGFloat(predictionResults.count)
        let index = min(predictionResults.count - 1, Int(CGFloat(max(0, scrollView.contentOffset.y + rowHeight / 2.0)) / scrollDelta))
        selectTag(index: index)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "error_handling_segue" {
            let navigationController = segue.destination as! UINavigationController
            let detailViewController = navigationController.topViewController as! DetailViewController
            detailViewController.prediction = self.selectedBeer
            detailViewController.resultOverrideDelegate = self
        }
    }
}

extension UIAlertController {

    private struct ActivityIndicatorData {
        static var activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 30, width: 60, height: 60))
    }

    func addActivityIndicator() {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 60,height: 120)
        let indicator = ActivityIndicatorData.activityIndicator
        indicator.style = .large
        indicator.startAnimating()
        vc.view.addSubview(indicator)
        self.setValue(vc, forKey: "contentViewController")
    }

    func dismissActivityIndicator(completion: (() -> Void)? = nil) {
        ActivityIndicatorData.activityIndicator.stopAnimating()
        self.dismiss(animated: false, completion: completion)
    }
}

//MARK: - Reporting

extension ResultViewController: HapticFeedbackProtocol {
    var hapticFeedbackGenerator: UISelectionFeedbackGenerator? {
        let generator = UISelectionFeedbackGenerator()
        generator.prepare()
        return generator
    }
    
    func registerInteraction() {
        hapticFeedbackGenerator?.selectionChanged()
    }
    
    func sendToExperts(prediction: MLManager.Prediction) {
        var json: [String: Any] = [:]
                
        // Results
        var results: [[String: Any]] = []
        for candidate in prediction.candidates {
            results.append([
                "category_id": candidate.category.id,
                "probability": candidate.confidence
            ])
        }
        json["top_categories"] = results
        json["tags"] = "sent_to_experts"
        json["user_id"] = 0
        
//        let data = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        
        POSTRequest(jsonObject: json, url: "https://bg-data.stage-tavour.com/rest/imageClassificationErrors", verbose: true, errorHandler: { (nil) in
            self.presentAlert(title: "Error", message: "Something went wrong. Please try again.")
        }) {
            self.presentAlert(title: "Success!", message: "The photo was sent to our beer nerds for manual matching.")
        }
    }
    
    func didLike(like: Bool, index: Int) {
        if let url = Bundle.main.url(forResource: "like", withExtension: "m4a") {
            player.removeAllItems()
            player.insert(AVPlayerItem(url: url), after: nil)
            player.play()
        }
        
        if !like {
            self.selectedIndex = index
            self.selectedBeer = predictionResults[index]
            self.performSegue(withIdentifier: "error_handling_segue", sender: self)
        } else {
            let prediction = predictionResults[index]
            submitToServer(errorType: "user_confirmed", user_id: 0, overrideID: prediction.candidates[0].category.id, breweryName: prediction.candidates[0].category.breweryName, beerName: prediction.candidates[0].category.beerName, verbose: false)
        }
    }
}
