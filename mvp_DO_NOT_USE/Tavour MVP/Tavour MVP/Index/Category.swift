//
//  Category.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 09.06.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import Foundation
import UIKit

struct Category {
    let id: Int
    let beerName: String
    let breweryName: String
    let anchorURL: String?
}
