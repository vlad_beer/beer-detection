//
//  UIViewController+BackendAPI.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 06.06.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

typealias JSON_t = [String: Any]

extension UIViewController {
    func POSTRequest(jsonObject: JSON_t? = nil,
                     url: String,
                     verbose: Bool = true,
                     errorHandler: ((String) -> Void)? = nil,
                     completionHandler: (() -> Void)? = nil) {

        if verbose {
            DispatchQueue.main.async {
                self.displayActivityIndicatorAlert()
            }
        }
        
        let data = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        
//        JSONSerialization.da
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        AF.request(request).responseJSON {
            (response) in
            print(response)
            if let status = response.response?.statusCode {
                switch(status){
                case 201:
                    print("example success")
                default:
                    print("error with response status: \(status)")
                }
            }
            if let data = response.data, let json = try? JSON.self(data: data), json["id"] != .null {
                if verbose {
                    self.dismissActivityIndicatorAlert {
                        completionHandler?()
                    }
                }
            } else {
                if verbose {
                    self.dismissActivityIndicatorAlert {
                        errorHandler?(response.description)
                    }
                }
            }
        }
        
//        let ulr =  NSURL(string:url)
//        let request = NSMutableURLRequest(url: ulr! as URL)
//        request.httpMethod = "POST"
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        let data = try! JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted)
//
//
//        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//        if let json = json {
//            print(json)
//        }
//        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
//
//
//        AF.request(request as! URLRequestConvertible)
//            .responseJSON { response in
//                // do whatever you want here
//               print(response.request)
//               print(response.response)
//               print(response.data)
//               print(response.result)
//
//        }
//
//        AF.request(url, method: HTTPMethod.post, parameters: jsonObject).responseJSON { response in
//            print(response)
//            self.dismissActivityIndicatorAlert {
//                completionHandler!()
//            }
//        }
        
//        AF.request(.POST, url, parameters: jsonObject, encoding: .JSON)
//        .responseJSON { request, response, JSON, error in
//            print("Response")
//            print(response)
////            print(JSON)
//            print("Error")
//            print(error)
//        }
        
//        let request = AFHTTPRequestSerializer().multipartFormRequest(withMethod: "POST", urlString: url, parameters: nil, constructingBodyWith: { (multipartFormData) in
//                if let image = image {
//                    let imageData = compress ? image.jpegData(compressionQuality: 1.0) : image.pngData()
//                    multipartFormData.appendPart(withFileData: imageData!, name: "image", fileName: "image.\(format)", mimeType: "image/\(format)")
//                }
//                if let message = message, let data = message.data(using: .utf8) {
//                    multipartFormData.appendPart(withForm: data, name: "json")
//                }
//                if let data = data {
//                    multipartFormData.appendPart(withForm: data, name: "json")
//                }
//            }, error: nil)
//
//        let manager = AFURLSessionManager(sessionConfiguration: .default)
//        manager.responseSerializer = AFJSONResponseSerializer()
//
//        let uploadTask: URLSessionUploadTask = manager.uploadTask(withStreamedRequest: request as URLRequest, progress: { (progress) in
//
//        }) { (response, responseObject, error) in
//            if verbose {
//                self.dismissActivityIndicatorAlert {
//                    if let _ = responseObject as? NSDictionary {
//                        print(responseObject)
//                        completionHandler?()
//                    } else {
//                        errorHandler?("Could not connect to the server.")
//                    }
//                }
//            }
//        }
        
//        uploadTask.resume()
    }
}
