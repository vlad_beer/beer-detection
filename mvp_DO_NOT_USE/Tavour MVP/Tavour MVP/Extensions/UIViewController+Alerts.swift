//
//  UIViewController+Alerts.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 06.06.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func presentAlert(title: String?, message: String?, terminate: Bool = false) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            if terminate {
                exit(0)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
}
