//
//  UIViewController+Loading.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 06.06.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    private static var _activityIndicatorAlert = [String : UIAlertController?]()
    
    var activityIndicatorAlert: UIAlertController? {
        get {
            return UIViewController._activityIndicatorAlert[
                String(format: "%p", unsafeBitCast(self, to: Int.self))] ?? nil
        }
        set(newValue) {
            UIViewController._activityIndicatorAlert[
                String(format: "%p", unsafeBitCast(self, to: Int.self))] = newValue
        }
    }

    func displayActivityIndicatorAlert() {
        activityIndicatorAlert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        activityIndicatorAlert!.addActivityIndicator()
        var topController: UIViewController = UIApplication.shared.windows.first!.rootViewController!
        while (topController.presentedViewController) != nil {
            topController = topController.presentedViewController!
        }
        topController.present(activityIndicatorAlert!, animated:true, completion:nil)
    }

    func dismissActivityIndicatorAlert(completion: (() -> Void)? = nil) {
        activityIndicatorAlert!.dismissActivityIndicator(completion: completion)
        activityIndicatorAlert = nil
    }
}
