//
//  DetailBeerDetailTableViewCell.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 06.06.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit

class DetailBeerDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var beerPhotoImageView: UIImageView!
    @IBOutlet weak var beerTitleLabel: UILabel!
    @IBOutlet weak var breweryTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension DetailBeerDetailTableViewCell {
    func setUp(data: MLManager.Prediction) {
        beerTitleLabel.text = data.candidates[0].category.beerName
        breweryTitleLabel.text = data.candidates[0].category.breweryName
        beerPhotoImageView.image = data.crop
    }
}
