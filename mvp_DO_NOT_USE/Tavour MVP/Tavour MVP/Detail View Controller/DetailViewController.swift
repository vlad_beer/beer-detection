//
//  DetailViewController.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 27.05.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var searchWrapper: UIStackView!
    
    var imageCache: [Int: UIImage] = [:]
    var appeared = false
    
    var prediction: MLManager.Prediction!

    var resultOverrideDelegate: ResultOverrideProtocol!
    
    private var supportedItems: [Int] = []

    var searchBarText: String = ""

    var searchBar: UISearchBar!

//    private func resetInfo() {
//        let candidate = prediction.candidates[0]
//        let components = candidate.className.components(separatedBy: " - ")
//        beerNameTextField.text = components[1]
//        breweryNameTextField.text = components[0]
//    }
//
//    private func setBeer(beerName: String, breweryName: String, image: UIImage? = nil) {
//        beerNameTextField.text = beerName
//        breweryNameTextField.text = breweryName
//
//        if let image = image {
//            beerImageView.image = image
//            resultOverrideDelegate.overrideResult(beerName: beerName, breweryName: breweryName, image: image)
//        }
//    }

    
    // MARK: - View Presentation & Loading
    override func viewDidLoad() {
        super.viewDidLoad()
    
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.separatorStyle = .none
        mainTableView.sectionHeaderHeight = 52
        
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchTableView.keyboardDismissMode = .onDrag
        searchTableView.separatorInset = UIEdgeInsets(top: 0, left: 66, bottom: 0, right: 0)
        searchTableView.sectionHeaderHeight = 0
        
        searchBar = UISearchBar()
        searchBar.searchBarStyle = .minimal
        searchBar.sizeToFit()
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.placeholder = "Search for this beer"
        navigationItem.titleView = searchBar
                
        searchWrapper.alpha = 0.0
        searchWrapper.isHidden = false
        
        searchTableView.backgroundColor = .white

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !appeared {
            navigationController?.setNavigationBarHidden(true, animated: false)
            appeared = true
        }
    }
}

extension DetailViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("[SEARCH] Cancelled")
        
        UIView.animate(withDuration: 0.15) {
            self.searchWrapper.alpha = 0.0
        }
        
        searchBar.text = ""
        searchBar.resignFirstResponder()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        print("[SEARCH] Should begin editing")
        
        self.searchTableView.reloadData()
        UIView.animate(withDuration: 0.3) {
            self.searchWrapper.alpha = 1.0
        }
        
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("[SEARCH] Searching: \(searchText)")
        
        let text = searchText.lowercased()
        searchBarText = text
        
        supportedItems = []
        if searchText.count > 0 {
            for i in 0..<MLManager.shared.supportedClasses.count {
                if MLManager.shared.supportedClasses[i].beerName.lowercased().contains(text) ||
                    MLManager.shared.supportedClasses[i].breweryName.lowercased().contains(text) {
                    supportedItems.append(i)
                }
            }
        }
        
        self.searchTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if (tableView == mainTableView) {
            return 3
        }
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == searchTableView {
            if supportedItems.count > 1 {
                return supportedItems.count + 2
            } else {
                return 0
            }
        } else {
            switch section {
            case 0:
                return 1
            case 1:
                return 1
            case 2:
                return 2
            default:
                return 0
            }
        }
    }
    
    func searchEntryCell(withTableView tableView: UITableView, forRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < supportedItems.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "candidate", for: indexPath) as! DetailTableViewCell
            
            let itemIndex = supportedItems[indexPath.row]
            let category = MLManager.shared.supportedClasses[itemIndex]
            
            cell.index = indexPath.row
            
            func fillMatches(text: String) -> NSAttributedString {
                let string = NSMutableAttributedString(string: text)
                if text.lowercased().contains(searchBarText.lowercased()) {
                    let range = text.lowercased().range(of: searchBarText.lowercased())!
                    let distance = text.distance(from: text.startIndex, to: range.lowerBound)
                    string.addAttribute(.backgroundColor, value: UIColor(red: 0.96, green: 0.96, blue: 1, alpha: 1), range: NSRange(location: distance, length: searchBarText.count))
                }
                return string
            }
            
            cell.breweryNameLabel.attributedText = fillMatches(text: category.breweryName)
            cell.beerNameLabel.attributedText = fillMatches(text: category.beerName)
            
            if cell.categoryID != category.id {
                cell.beerPhotoImageView.image = nil
                if imageCache[category.id] != nil {
                    cell.beerPhotoImageView.image = imageCache[category.id]
                } else {
                    DispatchQueue.global().async {
                        if let anchorURL = category.anchorURL, let url = URL(string: anchorURL),
                            let data = try? Data(contentsOf: url),
                            let image = UIImage(data: data) {
                            DispatchQueue.main.async {
                                cell.beerPhotoImageView.image = image
                                self.imageCache[category.id] = image
                            }
                        }
                    }
                }
            }
            cell.categoryID = category.id
        
            return cell
        } else if indexPath.row == supportedItems.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "create_new_beer", for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "send_to_experts", for: indexPath)
            return cell
        }
    }

    // MARK: - Main table view cells
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == searchTableView {
            return nil
        }
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: mainTableView.sectionHeaderHeight))
        if [1, 2].contains(section) {
            let title = ["", "Change Beer", "Actions"][section]
            let label = UILabel()
            label.text = title
            label.font = UIFont(name: "HelveticaNeue-Bold", size: 26)
            label.sizeToFit()
            var frame = label.frame
            frame.origin.y = headerView.frame.height - label.frame.height - 5
            if section == 1 {
                frame.origin.y -= 6
            } else if section == 2 {
                frame.origin.y += 2
            }
            frame.origin.x = 20
            label.frame = frame
            headerView.addSubview(label)
        }

        return headerView
    }
    
    @objc func selectedFromSuggestions(id: Int) {
        let category = MLManager.shared.category(id: id)
        didSelectBeer(categoryID: id, beerName: category.beerName, breweryName: category.breweryName)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == searchTableView {
            return searchEntryCell(withTableView: tableView, forRowAt: indexPath)
        } else {
            let section = indexPath.section
            let row = indexPath.row
            if section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "beer_detail", for: indexPath)
                    as! DetailBeerDetailTableViewCell
                cell.setUp(data: prediction)
                let content = cell.contentView
                let separator = UIView()
                let offset = CGFloat(18)
                separator.frame = CGRect(x: offset, y: content.frame.height - 0.5, width: content.frame.width - offset, height:0.5)
                separator.backgroundColor = UIColor(white: 0.9, alpha: 1)
                cell.contentView.addSubview(separator)
                return cell
            } else if section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "suggestions", for: indexPath)
                for view in cell.subviews {
                    if let view = view as? UIScrollView {
                        view.removeFromSuperview()
                    }
                }
                let scrollView = UIScrollView(frame: cell.contentView.bounds)
                let tileWidth = CGFloat(260)
                scrollView.contentSize = CGSize(width: CGFloat(prediction.candidates.count - 1) * tileWidth, height: scrollView.frame.height)
                var views: [SuggestionView] = []
                for i in 1..<prediction.candidates.count {
                    let view = SuggestionView(frame: CGRect(x: CGFloat(i - 1) * tileWidth, y: 0, width: tileWidth, height: scrollView.contentSize.height))
                    let category = prediction.candidates[i].category
                    view.beerTitle.text = category.beerName
                    view.breweryTitle.text = category.breweryName
                    view.imageView.contentMode = .scaleAspectFill
                    view.imageView.clipsToBounds = true
                    view.categoryID = category.id
                    view.selectButton.isUserInteractionEnabled = true
                    scrollView.isUserInteractionEnabled = true
                    
                    DispatchQueue.global().async {
                        if let anchorURL = category.anchorURL, let url = URL(string: anchorURL),
                            let data = try? Data(contentsOf: url),
                            let image = UIImage(data: data) {
                            DispatchQueue.main.async {
                                view.imageView.image = image
                            }
                        }
                    }
                    
                    scrollView.addSubview(view)
                    views.append(view)
                }
                scrollView.showsHorizontalScrollIndicator = false
                scrollView.isScrollEnabled = true
                cell.contentView.addSubview(scrollView)
                return cell
            } else { // section == 2
                if row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "retake", for: indexPath)
                        as! DetailRetakePhotoTableViewCell
                    let content = cell.contentView
                    let separator = UIView()
                    let offset = CGFloat(90)
                    separator.frame = CGRect(x: offset, y: content.frame.height - 0.5, width: content.frame.width - offset, height:0.5)
                    separator.backgroundColor = UIColor(white: 0.9, alpha: 1)
                    cell.contentView.addSubview(separator)
                    return cell
                } else { // indexPath.row == 1
                    let cell = tableView.dequeueReusableCell(withIdentifier: "search", for: indexPath)
                        as! DetailSearchTableViewCell
                    return cell
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if tableView == searchTableView {
            return indexPath.row >= supportedItems.count
        } else {
            if [IndexPath(row: 0, section: 2), IndexPath(row: 1, section: 2)].contains(indexPath) {
                return true
            }
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == mainTableView {
            if indexPath.section == 0 {
                return 122
            } else if indexPath.section == 1 {
                return 192
            } else {
                return 98
            }
        } else {
            return 78
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == searchTableView) {
            if indexPath.row == supportedItems.count {
                performSegue(withIdentifier: "create_new_beer", sender: self)
            } else if indexPath.row == supportedItems.count + 1 {
                self.submitToServer(errorType: "sent_to_experts", overrideID: nil, breweryName: "", beerName: "")
            }
        } else {
            if indexPath == IndexPath(row: 0, section: 2) {
                retakePhoto()
            } else if indexPath == IndexPath(row: 1, section: 2) {
                search()
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension DetailViewController: UIImagePickerControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info:
                                [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else {
            picker.dismiss(animated: true)
            return
        }

        picker.dismiss(animated: true) {
            self.displayActivityIndicatorAlert()

            DispatchQueue.global().async {
                let predictions = MLManager.shared.predict(image: image)
                
                if predictions.count > 0 {
                    let bestPrediction = predictions.min { (A, B) -> Bool in
                        let center = CGPoint(x: image.size.width / 2.0, y: image.size.height / 2.0)
                        let centerA = CGPoint(x: A.location.midX, y: A.location.midY)
                        let centerB = CGPoint(x: B.location.midX, y: B.location.midY)

                        return centerA.squaredDistance(to: center) < centerB.squaredDistance(to: center)
                    }!

                    self.prediction = bestPrediction
                    DispatchQueue.main.async {
                        self.dismissActivityIndicatorAlert {
                            self.resultOverrideDelegate.overrideResult(beerName: bestPrediction.candidates[0].category.beerName, breweryName: bestPrediction.candidates[0].category.breweryName)
                            self.mainTableView.reloadData()
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.dismissActivityIndicatorAlert() {
                            self.presentAlert(title: "No Beers Detected", message: "Could not detect any beers. Please try again.")
                        }
                    }
                }
                

            }
        }
    }
}

extension DetailViewController {
    func retakePhoto() {
        let picker = UIImagePickerController()
        picker.allowsEditing = false
        picker.delegate = self
        picker.sourceType = .camera
        present(picker, animated: true)
    }
    
    func search() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        supportedItems = []
        searchBar.becomeFirstResponder()
    }
    
    func overrideWithID(id: String) {
        
    }
}

extension DetailViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "create_new_beer" {
            let createNewBeerViewController = segue.destination as! CreateNewBeerViewController
            createNewBeerViewController.delegate = self
        }
    }
}

extension DetailViewController: BeerCreationProtocol {
    func submitToServer(errorType: String, user_id: Int = 0, overrideID: Int? = nil, breweryName: String, beerName: String) {
        var json: [String: Any] = [:]
        
        // Results
        var results: [[String: Any]] = []
        for i in 0..<prediction.candidates.count {
            let candidate = prediction.candidates[i]
            results.append([
                "category_id": candidate.category.id,
                "confidence": candidate.confidence
            ])
        }
        json["top_categories"] = results
        json["image_encoding"] = prediction.crop.toBase64()!
        json["user_id"] = 0
        json["error_type"] = errorType
        
        var additionalData: [String: Any] = [:]
        if let overrideID = overrideID {
            additionalData["override_id"] = overrideID
        } else if errorType == "beer_created" {
            additionalData["user_specified_brewery"] = breweryName
            additionalData["user_specified_beer"] = beerName
        }
        json["additional_data"] = additionalData
        
        POSTRequest(jsonObject: json, url:  "https://bg-data.stage-tavour.com/rest/imageClassificationErrors", verbose: true, errorHandler: { (message) in
            self.presentAlert(title: "Error", message: "Please try again later.")
        }) {
            self.resultOverrideDelegate.overrideResult(beerName: beerName, breweryName: breweryName)
            self.dismiss(animated: true) {
                (self.resultOverrideDelegate as! UIViewController).presentAlert(title: "Thanks!", message: "Thanks for helping make our service great!")
            }
        }
    }
    
    func didSelectBeer(categoryID: Int, beerName: String, breweryName: String) {
        submitToServer(errorType: "user_override", overrideID: categoryID, breweryName: breweryName, beerName: beerName)
    }
    
    func didCreateBeer(beerName: String, breweryName: String) {
        submitToServer(errorType: "beer_created", breweryName: breweryName, beerName: beerName)
    }
}
