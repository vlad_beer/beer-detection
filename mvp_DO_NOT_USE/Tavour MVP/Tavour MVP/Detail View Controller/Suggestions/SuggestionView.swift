//
//  SuggestionView.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 09.06.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit

class SuggestionView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var breweryTitle: UILabel!
    @IBOutlet weak var beerTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var categoryID: Int!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initFromNib()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initFromNib()
    }
    
    @IBAction func selected(_ sender: Any) {
        (self.parentViewController as! DetailViewController).selectedFromSuggestions(id: categoryID)
    }
    
    private func initFromNib() {
        Bundle.main.loadNibNamed("Suggestion", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
}
