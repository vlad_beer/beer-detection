//
//  CreateNewBeerViewController.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 06.06.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit

class CreateNewBeerViewController: UIViewController {
    @IBOutlet weak var inputTableView: UITableView!
    
    var delegate: BeerCreationProtocol!
    
    @objc func addBeer() {
        let breweryName = (inputTableView.cellForRow(at: IndexPath(row: 0, section: 0))
            as! MainUserInputTableViewCell).input
        let beerName = (inputTableView.cellForRow(at: IndexPath(row: 1, section: 0))
            as! MainUserInputTableViewCell).input
        
        delegate.didCreateBeer(beerName: beerName!, breweryName: breweryName!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        inputTableView.delegate = self
        inputTableView.dataSource = self
        
        navigationItem.title = "Add Beer"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(addBeer))
        navigationItem.rightBarButtonItem?.isEnabled = false

        // Do any additional setup after loading the view.
    }
    
    private func updateExitAvailability() {
        let breweryName = (inputTableView.cellForRow(at: IndexPath(row: 0, section: 0))
            as! MainUserInputTableViewCell).input
        let beerName = (inputTableView.cellForRow(at: IndexPath(row: 1, section: 0))
            as! MainUserInputTableViewCell).input
        
        if let breweryName = breweryName, let beerName = beerName,
            breweryName.count > 0, beerName.count > 0 {
            navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreateNewBeerViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Required"
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "user_input_main", for: indexPath)
                as! MainUserInputTableViewCell
            if indexPath.row == 0 {
                cell.setUp(with: "Brewery", textPlaceholder: "e.g. Anderson Valley")
            } else { // 1
                cell.setUp(with: "Beer Name", textPlaceholder: "e.g. Briney Melon Gose")
            }
            cell.userInputTextField.delegate = self
            return cell
        }
        return UITableViewCell()
    }
}

extension CreateNewBeerViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    public func textFieldDidChangeSelection(_ textField: UITextField) {
        updateExitAvailability()
    }
}
