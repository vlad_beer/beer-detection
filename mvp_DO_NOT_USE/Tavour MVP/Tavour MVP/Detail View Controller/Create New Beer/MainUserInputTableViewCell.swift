//
//  MainUserInputTableViewCell.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 06.06.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit

class MainUserInputTableViewCell: UITableViewCell {
    @IBOutlet weak var inputTypeLabel: UILabel!
    @IBOutlet weak var userInputTextField: UITextField!
    
    var input: String? {
        return userInputTextField.text
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setUp(with inputType: String, textPlaceholder: String) {
        inputTypeLabel.text = inputType
        userInputTextField.placeholder = textPlaceholder
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
