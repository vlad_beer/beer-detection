//
//  DetailTableViewCell.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 27.05.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {
    @IBOutlet weak var beerNameLabel: UILabel!
    @IBOutlet weak var breweryNameLabel: UILabel!
    @IBOutlet weak var selectCandidateButton: UIButton!
    @IBOutlet weak var beerPhotoImageView: UIImageView!
    
    var index: Int = -1
    var categoryID: Int = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func selectCandidate(_ sender: Any) {
        assert(index >= 0)
        
        (parentViewController as! DetailViewController).didSelectBeer(categoryID: categoryID, beerName: beerNameLabel.text!, breweryName: breweryNameLabel.text!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
