//
//  ResultOverrideProtocol.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 27.05.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import Foundation
import UIKit

protocol ResultOverrideProtocol {
    func overrideResult(beerName: String, breweryName: String)
}
