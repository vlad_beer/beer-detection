//
//  HapticFeedbackProtocol.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 31.05.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import Foundation
import UIKit

protocol HapticFeedbackProtocol {
    var hapticFeedbackGenerator: UISelectionFeedbackGenerator? { get }
    
    func registerInteraction()
    func playChimes()
    func didLike(like: Bool, index: Int)
    func didRate(index: Int, rating: Float)
}
