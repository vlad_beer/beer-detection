//
//  BeerCreationProtocol.swift
//  Tavour MVP
//
//  Created by Vladislav Shakhray on 06.06.2020.
//  Copyright © 2020 Vladislav Shakhray. All rights reserved.
//

import Foundation

protocol BeerCreationProtocol {
    func didCreateBeer(beerName: String, breweryName: String)
}
