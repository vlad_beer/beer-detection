## YOLOv3 Beer Detection

Please navigate to `app/` for the iOS MVP.

Navigate to `yolo+conversion` for more information on converting YOLOv3 models to CoreML.


The weights **required** for running the app can be downloaded from https://drive.google.com/open?id=15m3398_JnulRS2wkN-mIyc2GytvthMG6.
