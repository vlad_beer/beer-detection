## YOLOv3 Beer Detection

Please navigate to `demo.ipynb` for the tutorial.

Before that, make sure you have the weights downloaded at https://drive.google.com/drive/folders/1NCTiVZwBBTw13Bucix7US66C-X1D-Mno?usp=sharing.
