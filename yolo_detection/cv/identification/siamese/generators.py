import numpy as np
import random
import threading
import tqdm

from ..io import ImgReader
from ..data_manager import DataManager

from keras import backend as K
from keras.utils import Sequence
from lap import lapjv # Linear Assignment Problem


class TrainingData(Sequence):
    LARGE_SCORE = 1e5

    def __init__(self, score, data_manager: DataManager, reader: ImgReader, steps=5, batch_size=64, num_splits=6):
        """
        @param score the cost matrix for the picture matching
        @param steps the number of epoch we are planning with this score matrix
        """
        super(TrainingData, self).__init__()
        self.score = -score  # Minimizing the score
        self.steps = steps
        self.batch_size = batch_size
        self.reader = reader
        self.data_manager = data_manager
        self.num_splits = num_splits

        for indices in self.data_manager.class2indices.values():
            for i in indices:
                for j in indices:
                    self.score[i, j] = np.inf # Eliminate potential pairing
        self.on_epoch_end()

    def __getitem__(self, index):
        start = self.batch_size * index
        end = min(start + self.batch_size, len(self.match) + len(self.unmatch))
        size = end - start
        assert size > 0

        imgs_first = np.zeros((size,) + self.reader.img_shape, dtype=K.floatx())
        imgs_second = np.zeros((size,) + self.reader.img_shape, dtype=K.floatx())
        target = np.zeros((size, 1), dtype=K.floatx())

        j = start // 2
        for i in range(0, size, 2):
            assert self.data_manager.tagged[self.match[j][0]] == self.data_manager.tagged[self.match[j][1]]
            imgs_first[i, :, :, :] = self.reader.read_for_training(self.match[j][0])
            imgs_second[i, :, :, :] = self.reader.read_for_training(self.match[j][1])

            imgs_first[i + 1, :, :, :] = self.reader.read_for_training(self.unmatch[j][0])
            imgs_second[i + 1, :, :, :] = self.reader.read_for_training(self.unmatch[j][1])

            target[i, 0] = 1
            target[i + 1, 0] = 0
            j += 1
        return [imgs_first, imgs_second], target

    def on_epoch_end(self):
        print("Solving LAP...")
        if self.steps <= 0:
            return  # Skip this on the last epoch.
        self.steps -= 1
        self.match = []
        self.unmatch = []

        num_threads = 6
        output = [None] * num_threads
        threads = []
        thread_input = [None] * num_threads
        thread_idx = 0
        batch = (self.score.shape[0] + (num_threads - 1)) // num_threads
        for i in range(num_threads):
            start = i * batch
            end = min(start + batch, self.score.shape[0])
            assert end <= self.score.shape[0]
            thread_input[i] = self.score[start:end, start:end]

        def worker(data_idx):
            _, _, x = lapjv(thread_input[data_idx])
            output[data_idx] = x

        for i in range(num_threads):
            t = threading.Thread(target=worker, args=(i,), daemon=True)
            t.start()
            threads.append(t)
        for t in threads:
            if t is not None:
                t.join()
        x = np.concatenate(output)
        assert x.shape[0] == len(self.data_manager), (x.shape[0], len(self.data_manager))
        y = np.arange(len(x), dtype=np.int32)

        for ts in self.data_manager.class2items.values():
            d = ts.copy()
            while True:
                random.shuffle(d)
                if not np.any(ts == d):
                    break
            for ab in zip(ts, d): self.match.append(ab)

        for i, j in zip(x, y):
            self.unmatch.append((self.data_manager.items[i], self.data_manager.items[j]))

        self.score[x, y] = self.LARGE_SCORE
        self.score[y, x] = self.LARGE_SCORE
        random.shuffle(self.match)
        random.shuffle(self.unmatch)
        assert len(self.match) == len(self.data_manager) and len(self.unmatch) == len(self.data_manager)
        print("Ended solving LAP.")

    def __len__(self):
        return (len(self.match) + len(self.unmatch) + self.batch_size - 1) // self.batch_size


# A Keras generator to evaluate only the BRANCH MODEL
class FeatureGen(Sequence):
    def __init__(self, data_manager: DataManager, reader: ImgReader, batch_size=256, verbose=1):
        super(FeatureGen, self).__init__()
        self.data_manager = data_manager
        self.reader = reader
        self.batch_size = batch_size
        self.verbose = verbose
        self.n = len(data_manager)
        if self.verbose > 0:
            self.progress = tqdm.tqdm_notebook(total=len(self), desc='Features')

    def __getitem__(self, index):
        start = self.batch_size * index
        size = min(len(self.data_manager) - start, self.batch_size)
        a = np.zeros((size,) + self.reader.img_shape, dtype=K.floatx())
        for i in range(size):
            a[i, :, :, :] = self.reader.read_for_validation(self.data_manager.items[start + i])

        if self.verbose > 0:
            self.progress.update()
            if self.progress.n >= len(self): self.progress.close()
        return a

    def __len__(self):
        return (len(self.data_manager) + self.batch_size - 1) // self.batch_size
   


# A Keras generator to evaluate on the HEAD MODEL on features already pre-computed.
# It computes only the upper triangular matrix of the cost matrix if y is None.
class ScoreGen(Sequence):
    def __init__(self, x, y=None, batch_size=2048, verbose=1):
        super(ScoreGen, self).__init__()
        self.x = x
        self.y = y
        self.batch_size = batch_size
        self.verbose = verbose
        if y is None:
            self.y = self.x
            self.ix, self.iy = np.triu_indices(x.shape[0], 1)
        else:
            self.iy, self.ix = np.indices((y.shape[0], x.shape[0]))
            self.ix = self.ix.reshape((self.ix.size,))
            self.iy = self.iy.reshape((self.iy.size,))
        self.subbatch = (len(self.x) + self.batch_size - 1) // self.batch_size
        if self.verbose > 0: self.progress = tqdm.tqdm_notebook(total=len(self), desc='Scores')

    def __getitem__(self, index):
        start = index * self.batch_size
        end = min(start + self.batch_size, len(self.ix))
        a = self.y[self.iy[start:end], :]
        b = self.x[self.ix[start:end], :]
        if self.verbose > 0:
            self.progress.update()
            if self.progress.n >= len(self): self.progress.close()
        return [a, b]

    def __len__(self):
        return (len(self.ix) + self.batch_size - 1) // self.batch_size