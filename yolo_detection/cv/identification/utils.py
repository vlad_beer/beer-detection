import dlib
import glob
import os


def _gen_rand(extension):
    import secrets
    return str(secrets.token_hex(16)) + extension


def reorganize_folder2(input_folder, output_folder, randomize_titles=True, extension='.jpg'):
    if extension[0] != '.':
        extension = '.' + extension

    samples = glob.glob(os.path.join(input_folder, '*/*'))
    tagged = dict()
    for sample in samples:
        img = dlib.load_rgb_image(sample)
        class_name = sample.split('/')[-2]
        filename = _gen_rand(extension) if randomize_titles else img.split('/')[-1]
        tagged[filename] = class_name
        dlib.save_image(img, os.path.join(output_folder, filename))
    return tagged
