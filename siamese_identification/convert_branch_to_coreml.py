import argparse
import coremltools
import os

from coremltools.proto import NeuralNetwork_pb2
from keras import backend as K
from keras.layers import Input, Lambda
from keras.models import Model, load_model
from cv.identification.siamese.utils import parse_model

def normalize(x):
    return (x - K.mean(x)) / (K.std(x))

def convert_lambda(layer):
    if layer.function == normalize:
        params = NeuralNetwork_pb2.CustomLayerParams()
        params.className = "Normalize"
        params.description = "Normalization function"
        return params
    return None

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=str)
    parser.add_argument('-o', '--output', type=str)
    args = parser.parse_args()
    return args.input, args.output

def main(input, output):
    assert os.path.exists(input)

    model = load_model(input, compile=False)
    branch_model, head_model = parse_model(model)
    branch_model.name = 'branch'

    branch_model.save('.branch.hdf5')
    branch_model = load_model('.branch.hdf5')
    os.remove('.branch.hdf5')

    inputs = Input(shape=branch_model.input_shape[1:], name='input')
    normalized = Lambda(normalize, name='lambda') (inputs)

    injected = Model(inputs, branch_model(normalized), name='result')

    coreml_model = coremltools.converters.keras.convert(
        injected,
        input_names="image",
        image_input_names="image",
        output_names="output",
        add_custom_layers=True,
        custom_conversion_functions={"Lambda": convert_lambda})
    
    coreml_model.save(output)
    print(coreml_model)
    print('Done')

if __name__ == '__main__':
    input, output = parse_args()
    main(input, output)
