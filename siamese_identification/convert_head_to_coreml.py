import argparse
import coremltools
import os

from coremltools.proto import NeuralNetwork_pb2
from keras import backend as K
from keras.layers import Input, Lambda, Concatenate, Conv2D, Reshape, Flatten, Dense
from keras.models import Model, load_model
from cv.identification.siamese.utils import parse_model

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=str)
    parser.add_argument('-o', '--output', type=str)
    args = parser.parse_args()
    return args.input, args.output

def f_mul(x):
    return x[0] * x[1]

def f_add(x):
    return x[0] + x[1]

def f_l1(x):
    return K.abs(x[0] - x[1])

def f_sq(x):
    return K.square(x)

def convert_lambda(layer):
    func_titles = {
        f_mul: 'FMul',
        f_add: 'FAdd',
        f_l1: 'FL1',
        f_sq: 'FSq'
    }
    
    assert layer.function in func_titles, layer.function
    params = NeuralNetwork_pb2.CustomLayerParams()
    params.className = func_titles[layer.function]
    return params

def build_head():
    mid = 32
    xa_inp = Input(shape=(512,))
    xb_inp = Input(shape=(512,))

    x1 = Lambda(f_mul, name='f_mul') ([xa_inp, xb_inp])
    x2 = Lambda(f_add, name='f_add') ([xa_inp, xb_inp])
    x3 = Lambda(f_l1, name='f_l1') ([xa_inp, xb_inp])
    x4 = Lambda(f_sq, name='f_sq')(x3)

    x = Concatenate()([x1, x2, x3, x4])
    x = Reshape((4, 512, 1), name='reshape1')(x)

    x = Conv2D(mid, (4, 1), activation='relu', padding='valid')(x)
    x = Reshape((512, mid, 1))(x)
    x = Conv2D(1, (1, mid), activation='linear', padding='valid')(x)
    x = Flatten(name='flatten')(x)

    x = Dense(1, use_bias=True, activation='sigmoid', name='weighted_average')(x)
    return Model([xa_inp, xb_inp], x, name='head')

def main(input, output):
    assert os.path.exists(input)

    model = load_model(input, compile=False)
    branch_model, head_model = parse_model(model)
    
    head = build_head()
    head.set_weights(head_model.get_weights())

    coreml_model = coremltools.converters.keras.convert(
        head,
        input_names=["features1", "features2"],
        output_names="probability",
        add_custom_layers=True,
        custom_conversion_functions={"Lambda": convert_lambda})
    
    coreml_model.save(output)
    print(coreml_model)
    print('Done')

if __name__ == '__main__':
    input, output = parse_args()
    main(input, output)
