import os
import random
import numpy as np

from PIL import Image
from tensorflow.keras import backend as K
from tensorflow.keras.preprocessing.image import img_to_array
from scipy.ndimage import affine_transform


class ImgReader:
    def __init__(self, img_shape, paths, anisotropy=1., crop_margin=0, cache_images=True):
        self.img_shape = img_shape
        self.paths = paths
        self.anisotropy = anisotropy
        self.crop_margin = crop_margin
        self.cache_images = cache_images

        if self.cache_images:
            self.cache = dict()

    def read_raw(self, p) -> Image:
        ext_path = self._expand_path(p)
        if self.cache_images and p in self.cache:
            return self.cache[p]
        elif self.cache_images:
            self.cache[p] = Image.open(ext_path)
            return self.cache[p]
        else:
            return Image.open(ext_path)

    def read_for_training(self, img):
        return self._read_cropped_image(img, augment=True)

    def read_for_validation(self, img):
        return self._read_cropped_image(img, augment=False)

    def _read_cropped_image(self, p, augment) -> Image:
        """
        @param p : the name of the picture to read
        @param augment: True/False if data augmentation should be performed
        @return a numpy array with the transformed image
        """
        img = self.read_raw(p)
        size_x, size_y = img.size
        
        if self.img_shape[2] == 1:
            img = img.convert('L')
        img = img_to_array(img)

        # Determine the region of the original image we want to capture based on the bounding box.
        x0, y0, x1, y1 = 0, 0, size_x - 1, size_y - 1
        dx = x1 - x0
        dy = y1 - y0
        if dx > dy * self.anisotropy:
            dy = 0.5 * (dx / self.anisotropy - dy)
            y0 -= dy
            y1 += dy
        else:
            dx = 0.5 * (dy * self.anisotropy - dx)
            x0 -= dx
            x1 += dx

        # Generate the transformation matrix
        trans = np.array([[1, 0, -0.5 * self.img_shape[0]], [0, 1, -0.5 * self.img_shape[1]], [0, 0, 1]])
        trans = np.dot(np.array([[(y1 - y0) / self.img_shape[0], 0, 0], [0, (x1 - x0) / self.img_shape[1], 0], [0, 0, 1]]), trans)
        if augment:
            trans = np.dot(self._build_transform(
                random.uniform(-5, 5),
                random.uniform(-5, 5),
                random.uniform(0.8, 1.0),
                random.uniform(0.8, 1.0),
                random.uniform(-0.15 * (y1 - y0), 0.1 * (y1 - y0)),
                random.uniform(-0.15 * (x1 - x0), 0.1 * (x1 - x0))
            ), trans)
        trans = np.dot(np.array([[1, 0, 0.5 * (y1 + y0)], [0, 1, 0.5 * (x1 + x0)], [0, 0, 1]]), trans)

        # Apply affine transformation
        matrix = trans[:2, :2]
        offset = trans[:2, 2]
        img = img.reshape(img.shape)
    
        img = np.dstack([affine_transform(img[:,:,i], matrix, offset, output_shape=self.img_shape[:-1], order=1, mode='constant',
                               cval=np.average(img[:,:,i])) for i in range(self.img_shape[2])])
        img = img.reshape(self.img_shape)

        # Normalize to zero mean and unit variance
        img -= np.mean(img, keepdims=True)
        img /= np.std(img, keepdims=True) + K.epsilon()
        return img

    @staticmethod
    def _build_transform(rotation, shear, height_zoom, width_zoom, height_shift, width_shift):
        """
        Build a transformation matrix with the specified characteristics.
        """
        rotation = np.deg2rad(rotation)
        shear = np.deg2rad(shear)
        rotation_matrix = np.array([
                [np.cos(rotation), np.sin(rotation), 0],
                [-np.sin(rotation), np.cos(rotation), 0],
                [0, 0, 1]])
        shear_matrix = np.array([[1, np.sin(shear), 0], [0, np.cos(shear), 0], [0, 0, 1]])
        zoom_matrix = np.array([[1.0 / height_zoom, 0, 0], [0, 1.0 / width_zoom, 0], [0, 0, 1]])
        shift_matrix = np.array([[1, 0, -height_shift], [0, 1, -width_shift], [0, 0, 1]])
        return np.dot(np.dot(rotation_matrix, shear_matrix), np.dot(zoom_matrix, shift_matrix))

    def _expand_path(self, p):
        for path in self.paths:
            ext_path = os.path.join(path, p)
            if os.path.exists(ext_path):
                return ext_path
        raise FileNotFoundError
