import random
from collections import defaultdict


class DataManager:
    def __init__(self, items, tagged, train=True):
        self.items = items
        self.tagged = tagged
        self.train = train
    
        self.item2index = dict()
        self.class2items = defaultdict(list)
        self.class2indices = defaultdict(list)

        self._recalculate()

    def __len__(self):
        return len(self.items)

    def reshuffle(self):
        random.shuffle(self.items)
        
        self._recalculate()

    def _recalculate(self, finish=False):
        self.item2index = dict()
        self.class2items = defaultdict(list)
        self.class2indices = defaultdict(list)

        for index, item in enumerate(self.items):
            self.item2index[item] = index

        for key, value in self.tagged.items():
            if key in self.item2index:
                self.class2items[value].append(key)
                self.class2indices[value].append(self.item2index[key])
        
        # Removing classes with 1 item
        if self.train and not finish:
            to_remove = []
            for key, values in self.class2items.items():
                if len(values) == 1:
                    to_remove.append(self.item2index[values[0]])

            self.items = [self.items[i] for i in range(len(self.items)) if i not in to_remove]
            self._recalculate(True)
