import numpy as np

from .generators import FeatureGen, ScoreGen

from tensorflow.keras.models import Model
from tensorflow.keras import backend as K


def parse_model(model: Model):
    return model.layers[-2], model.layers[-1]


def set_lr(model, lr):
    K.set_value(model.optimizer.lr, float(lr))


def get_lr(model):
    return K.get_value(model.optimizer.lr)


def score_reshape(score, x, y=None):
    """
    Transformed the packed matrix 'score' into a square matrix.
    @param score the packed matrix
    @param x the first image feature tensor
    @param y the second image feature tensor if different from x
    @result the square matrix
    """
    if y is None:
        # When y is None, score is a packed upper triangular matrix.
        # Unpack, and transpose to form the symmetrical lower triangular matrix.
        m = np.zeros((x.shape[0],x.shape[0]), dtype=K.floatx())
        m[np.triu_indices(x.shape[0],1)] = score.squeeze()
        m += m.transpose()
    else:
        m = np.zeros((y.shape[0],x.shape[0]), dtype=K.floatx())
        iy, ix = np.indices((y.shape[0],x.shape[0]))
        ix = ix.reshape((ix.size,))
        iy = iy.reshape((iy.size,))
        m[iy,ix] = score.squeeze()
    return m

def compute_score(model, data_manager, reader, verbose=1):
    """
    Compute the score matrix by scoring every pictures from the training set against the pictures from the other set O(n^2).
    """   
    branch_model, head_model = parse_model(model)
    features = branch_model.predict_generator(FeatureGen(data_manager, reader, verbose=verbose), max_queue_size=32, workers=8, verbose=0)
    score = head_model.predict_generator(ScoreGen(features, features, verbose=verbose), max_queue_size=32, workers=8, verbose=0)
    score = score_reshape(score, features, features)
    return features, score

def log(msg, model_name):
    f = open(f'logs/{model_name}.txt', 'a+')
    print(msg)
    f.write(msg + '\n')
    f.close()

def evaluate_accuracy(branch_model, head_model, test_manager, train_manager, reader, score=None):
    if score is None:
        test_features = branch_model.predict_generator(FeatureGen(test_manager, reader, verbose=True), max_queue_size=16, workers=12, verbose=0)
        train_features = branch_model.predict_generator(FeatureGen(train_manager, reader, verbose=True), max_queue_size=16, workers=12, verbose=0)
        score_flat = head_model.predict_generator(ScoreGen(test_features, train_features, verbose=True), max_queue_size=32, workers=12, verbose=0)
        score = score_reshape(score_flat, test_features, train_features).T
    
    metric = 0
    for i in range(len(test_manager)):
        index = np.argmax(score[i])
        if test_manager.tagged[test_manager.items[i]] == train_manager.tagged[train_manager.items[index]]:
            metric += 1
    return metric / len(test_manager)
