import os

import numpy as np

from .utils import parse_model, get_lr, set_lr, compute_score, log, evaluate_accuracy
from .generators import FeatureGen, ScoreGen, TrainingData
from ..data_manager import DataManager

from keras_tqdm import TQDMNotebookCallback
from keras.callbacks import CSVLogger, ModelCheckpoint


class Trainer:
    def __init__(self, model, model_name, reader, train_manager, val_manager, verbose):
        self.model = model
        self.model_name = model_name
        self.branch_model, self.head_model = parse_model(self.model)
        self.reader = reader
        self.train_manager = train_manager
        self.val_manager = val_manager
        self.verbose = verbose

        self.steps = 0
        self.history = []

        for folder in ['logs', 'models']:
            os.makedirs(folder, exist_ok=True)
        os.makedirs(f'models/{model_name}', exist_ok=True)
    
    def train(self, lr, amplitude, num_blocks, steps_per_block, batch_size=32, enable_lr_flip=False):
        set_lr(self.model, lr)

        for _ in range(num_blocks):
            self.train_manager.reshuffle()

            # Computing score matrix
            features, score = compute_score(self.model, self.train_manager, self.reader, verbose=self.verbose)
            
            # Computing training accuracy
            train_accuracy = evaluate_accuracy(self.branch_model, self.head_model, self.train_manager, self.train_manager, self.reader, score=score)
            log(f'step:{self.steps}\ttrain_accuracy\t{train_accuracy}', self.model_name)
            
            # Training the model for 'step' epochs
            history = self.model.fit_generator(
                TrainingData(score + amplitude * np.random.random_sample(size=score.shape),
                            self.train_manager,
                            self.reader,
                            steps=steps_per_block,
                            batch_size=batch_size),
                initial_epoch=self.steps, epochs=self.steps+steps_per_block, max_queue_size=12, workers=8, verbose=0,
                callbacks=[
                    TQDMNotebookCallback(leave_inner=True, metric_format='{value:0.3f}'),
                    CSVLogger('logs/' + self.model_name + '.csv', append=True, separator=';'),
                    ModelCheckpoint('models/' + self.model_name + '/weights.{epoch:02d}-{acc:.3f}.hdf5',
                                                    monitor='acc', save_best_only=False, save_weights_only=False)
                ]).history
            
            # Logging mean score
            mean_score = score.mean()
            log(f'step:{self.steps}\tscore_mean\t{mean_score}', self.model_name)

            # Collect history data
            self.steps += steps_per_block
            history['epochs'] = steps
            history['ms'] = mean_score
            history['lr'] = get_lr(self.model)
            
            self.history.append(history)
            
            test_accuracy = evaluate_accuracy(self.branch_model, self.head_model, self.test_manager, self.train_manager, self.reader)
            log(f'step:{steps}\ttest_accuracy\t{test_accuracy}', self.model_name)