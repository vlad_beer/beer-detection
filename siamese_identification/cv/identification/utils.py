import dlib
import glob
import tqdm
import os


def _gen_rand(extension):
    import secrets
    return str(secrets.token_hex(16)) + extension


def reorganize_folder2(input_folder, output_folder, randomize_titles=True, extension='.jpg', verbose=False):
    if extension[0] != '.':
        extension = '.' + extension

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
        
    samples = glob.glob(os.path.join(input_folder, '*/*'))
    tagged = dict()
    inverse_mapping = dict()
    for sample in tqdm.tqdm_notebook(samples) if verbose else samples:
        img = dlib.load_rgb_image(sample)
        class_name = sample.split('/')[-2]
        filename = _gen_rand(extension) if randomize_titles else img.split('/')[-1]
        tagged[filename] = class_name
        inverse_mapping[sample] = filename
        dlib.save_image(img, os.path.join(output_folder, filename))
    return tagged, inverse_mapping
